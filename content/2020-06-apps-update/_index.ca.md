---
layout: page
publishDate: 2020-06-11 12:00:00
summary: "Qu\xE8 ha passat a les aplicacions del KDE aquest mes"
title: "Actualitzaci\xF3 de les aplicacions del KDE de juny de 2020"
type: announcement
---
Sempre és joiós quan creix la família KDE. Aquest mes estem especialment feliços de donar la benvinguda al gestor de còpies de seguretat Kup i a un esforç d'empaquetament nou: Homebrew.

# Llançaments nous

## Kup 0.8

El [Kup](https://store.kde.org/p/1127689) és una eina de còpies de seguretat per a mantenir segurs els vostres fitxers.

Anteriorment estava desenvolupat fora del KDE, però aquest darrer mes ha passat el procés d'incubació i s'ha unit a la nostra comunitat, esdevenint oficialment un projecte KDE. El desenvolupador líder, el Simon Persson, ho ha celebrat amb un llançament nou.

Aquí hi ha els canvis que trobareu a la versió nova:

* Ha canviat com s'emmagatzemen les còpies de seguretat de tipus «rsync» quan només se selecciona una carpeta origen. Aquest canvi intenta minimitzar el risc de suprimir fitxers d'un usuari que selecciona una carpeta no buida com a destinació. S'ha afegit codi de migració per a detectar i moure fitxers en la primera execució, i per tant, evitant copiar-ho tot un altre cop i duplicant l'emmagatzematge.

* S'ha afegit una opció avançada que permet especificar un fitxer que el Kup llegirà per a excloure patrons. Per exemple, permet indicar que el Kup mai desi els fitxers *.bak.

* Han canviant els paràmetres predeterminats, esperem que siguin millors.

* S'han reduït els avisos quant als fitxers que no s'inclouen, ja que donaven massa alarmes falses.

* El Kup ja no demana la contrasenya per a desbloquejar les unitats externes encriptades només per a mostrar quant espai hi ha disponible.

* S'ha corregit el tractament d'un desament de còpia de seguretat com a fallit només perquè hi ha fitxers que mancaven durant l'operació, tant per al «rsync» com el «bup».

* S'han iniciat les comprovacions i reparacions de la integritat de les còpies de seguretat en paral·lel, en funció del nombre de CPU.

* S'ha afegit el funcionament per a la versió 3 de les metadades del «bup», el qual s'ha incorporat a la versió 0.30 del «bup».

* Moltes correccions petites a la interfície d'usuari.

El Kup pot fer les còpies de seguretat usant el «rsync» o fer còpies de seguretat versionades amb l'eina Bup en Python. Actualment el Bup només funciona amb el Python 2, i per tant, aquesta opció no està disponible a moltes distribucions, però s'està treballant en l'adaptació al Python 3.

{{< img class="text-center" src="kup.png" caption="Kup" style="width: 600px">}}

Per a trobar més informació quant al Kup, no fa gaire l'[Average Linux User va fer una anàlisi](https://averagelinuxuser.com/kup-backup/) i un vídeo del Kup:

{{< youtube y53IE0G8Brg >}}

# El Krita en tauletes Android

Gràcies al treball intens del Sharaf Zaman, ara el Krita està disponible a la Play Store de Google per a les tauletes Android i els Chromebook (però no per als telèfons Android).

Aquesta versió beta, basada en el Krita 4.2.9, és la versió completa d'escriptori del Krita, i per tant, no té una interfície tàctil especial d'usuari. Però està aquí, i podeu provar-la.

A diferència de les botigues Windows i Steam, la botiga no demana diners per al Krita, ja que és la única manera que la gent pugui instal·lar el Krita en aquests dispositius. Tanmateix, podreu comprar una insígnia de col·laboració del Krita per a ajudar amb el seu desenvolupament.

Per a instal·lar

* Obteniu el [Krita des de Google Play](https://play.google.com/store/apps/details?id=org.krita)

* De manera alternativa, a la Play Store, canvieu a la pestanya «Early access» i cerqueu org.krita. (Vegeu les instruccions oficials de Google per a l'Early Access. Fins que no s'aconsegueixi un nombre raonable de baixades, caldrà desplaçar-se cap avall)

* També podeu [baixar els fitxers «apk» vós mateix](https://files.kde.org/krita/android/). NO demaneu ajuda per a instal·lar aquests fitxers.

* Tots aquests són els llocs oficials. No instal·leu el Krita des d'altres fonts, no podem garantir la seva seguretat.

Notes

* Admet tauletes Android i Chromebooks. Versions acceptades de l'Android: Android 6 (Marshmallow) i superiors.

* Actualment no compatible amb: telèfons Android.

* Si heu instal·lat una de les compilacions del Sharaf o un compilació que heu signat vós mateix, primer cal desinstal·lar aquesta, per a tots els usuaris!

{{< img class="text-center" src="krita-android.jpg" caption="El Krita a l'Android" style="width: 600px">}}

# Entrades

El [KIO Fuse](https://techbase.kde.org/Projects/KioFuse) ha efectuat el seu primer [llançament beta](https://mail.kde.org/pipermail/kde-announce-apps/2020-May/005587.html) aquest mes.

## Correcció d'errors

S'han publicat llançaments de correcció d'errors per a

* El gestor de col·leccions [Tellico](https://mail.kde.org/pipermail/kde-announce-apps/2020-June/005588.html) amb un diàleg de filtre actualitzat que permet coincidències amb text buit.

* L'explorador de la xarxa local [SMB4K](https://sourceforge.net/p/smb4k/blog/2020/06/smb4k-306-released/) ha corregit el desament de la configuració en tancar.

* L'IDE del programadors [KDevelop](https://www.kdevelop.org/news/kdevelop-552-released) ha efectuat una actualització per als repositoris del KDE que s'han mogut.

# Botigues d'aplicacions

{{< img class="text-center" src="homebrew.png" caption="Homebrew" style="width: 391px">}}

Encara que al Linux ens estem acostumant gradualment a poder instal·lar aplicacions individuals d'una botiga d'aplicacions, a la inversa està passant en el món del macOS i el Windows. Per a aquests sistemes, els gestors de paquets s'estan introduint entre els que volen una font per a controlar-ho tot als seus sistemes.

El repositori líder de paquets de codi font obert per a macOS és Homebrew, gestionat per un equip de desenvolupadors de primera que inclou l'antic desenvolupador de KDE, en Mike McQuaid.

Aquest mes el projecte [KDE Homebrew](https://invent.kde.org/packaging/homebrew-kde), el qual ha estat funcionant de manera externa al KDE fins ara, s'ha mogut dins el KDE per a ser una part completa de la nostra comunitat.

Podeu afegir el repositori KDE Homebrew per a macOS i baixar els codis font del KDE compilats i preparats per a que l'executeu.

Hem quedat amb el desenvolupador principal, en Yurii Kolesnykov i li hem preguntat pel projecte.

### Parleu-nos sobre vós mateix, el nom, d'on veniu, quins són els vostres interessos en el KDE i el Mac, què feu per a viure?

El meu nom és Yurii Kolesnykov, i sóc d'Ucraïna. Tinc una passió pel programari lliure des de la primera vegada que vaig sentir parlar d'ell, al final de l'institut aproximadament. Penso que KDE és el millor entorn d'escriptori per als sistemes Linux i Unix, amb moltes aplicacions importants. El meu interès pel Mac ve de la meva feina principal, jo desenvolupo programari mòbil de l'iOS per a viure.

### Què és el Homebrew?

El Homebrew és el gestor de paquets més popular per a macOS, com l'«apt» o el «yum». Com que el macOS és Unix i Apple proporciona un bon compilador i una cadena d'eines per a ell, la gent va decidir crear gestors de paquets. D'aquesta manera es pot instal·lar molt programari lliure i de codi obert al Mac. Homebrew també té un subprojecte, anomenat Homebrew Cask, el qual permet instal·lar moltes aplicacions executables, és a dir, propietàries o gràfiques. Perquè les aplicacions gràfiques són complicades d'integrar amb el sistema si s'instal·len amb Homebrew.

### Quins paquets del KDE heu fet per al Homebrew?

Acabo d'executar un «grep» al nostre «tap» i he vist que hi ha 110 paquets en total, 67 dels quals són Frameworks, i aproximadament 39 aplicacions. Ja tenim les aplicacions més populars, com el Kate, Dolphin i KDevelop, gràcies a les peticions dels usuaris.

### Com a usuari de Mac, què cal per a aconseguir instal·lar aplicacions?

Per a començar, cal seguir la guia d'instal·lació de Homebrew si encara no ho heu fet. Està disponible a [brew.sh](https://brew.sh). Després cal fer «tap» del repositori amb el següent:

`brew tap kde-mac/kde https://invent.kde.org/packaging/homebrew-kde.git`

Malauradament hi ha diversos paquets KDE que no funcionen per si mateixos, però hem creat un script que fa tota la feina necessària, així que després de fer «tap» cal executar l'ordre següent:

`"$(brew --repo kde-mac/kde)/tools/do-caveats.sh"`

### Coneixeu com és de popular Homebrew per a aconseguir aplicacions per al Mac?

Bona pregunta. Malauradament no hem configurat cap analítica encara, i ho afegeixo a la meva llista de tasques pendents. Però dono per fet que Homebrew és el gestor de paquets més popular per al Mac i que requereix que els usuaris no el barregin amb altres projectes semblants per a instal·lar programari al mateix Mac per a evitar conflictes. Per tant, sí, penso que és força popular.

### Quina feina cal fer per a aconseguir que les aplicacions KDE funcionin a Homebrew?

Durant la creació dels paquets actuals ja hem corregit molts problemes habituals, així que portar programari nou és relativament fàcil. Prometo escriure un «Com fer» per a això, els usuaris ho han demanat moltes vegades.

### Actualment cal compilar localment els paquets. Tindreu paquets precompilats disponibles?

Homebrew permet instal·lar programari a través de Bottles, és a dir, paquets binaris precompilats. Però el procés de crear «bottles» està íntimament integrat amb la infraestructura de Homebrew, i cal executar CI amb proves de cada paquet abans que es creï la «bottle». De manera que es va decidir integrar tants paquets com fos possible al repositori «brew» principal per a eliminar càrrega de manteniment.

### Hi ha altre programari d'escriptori disponible a Homebrew?

Sí. En general, si una aplicació és popular i té un canal de distribució fora de la Mac AppStore, hi ha una alta probabilitat que ja estigui disponible per a instal·lar a través d'un Brew Cask.

### Com poden ajudar els autors d'aplicacions KDE per a que el seu programari estigui a Homebrew?

El maquinari d'Apple és molt car, per tant, facilitar un Mac per a cada desenvolupador del KDE no seria una bona idea. Així que per ara s'agraeix que creïn una Petició de funcionalitat en el nostre repositori. Llavors els mantenidors o els usuaris de Homebrew KDE informen d'errors si quelcom no està funcionant com s'esperava. I nosaltres intentem proporcionar tanta informació com sigui possible a petició dels desenvolupadors del KDE. Però per ara tenim diversos tiquets pendents per a les aplicacions KDE amb errors petits però molt molestos. Espero que estarem més integrats amb la infraestructura del KDE i, per tant, podrem enllaçar els errors al nostre repositori amb els projectes de la línia principal. Nosaltres ja hem migrat al KDE Invent i espero que KDE Bugs es migri aviat des del Bugzilla al KDE Invent.

### L'altra manera d'obtenir aplicacions KDE construïdes per al Mac és amb Craft. Com es comparen les aplicacions construïdes al Homebrew amb les construïdes al Craft?

Encara penso que Homebrew és més fàcil d'emprar per als usuaris finals. El seu procés d'instal·lació és tant fàcil com executar una línia. Per a afegir el nostre repositori i començar a instal·lar les seves aplicacions, cal executar dues altres línies.

### Gràcies pel teu temps, Yurii.

# Llançament 20.04.2

Diversos projectes publiquen en la seva pròpia escala de temps i altres es publiquen conjuntament. El paquet de projectes 20.04.2 s'ha publicat avui i aviat estarà disponible a través de les botigues d'aplicacions i de les distribucions. Vegeu la [pàgina del llançament 20.04.2](https://www.kde.org/info/releases-20.04.2.php) per als detalls.

Algunes de les correccions al llançament d'avui:

* Les escriptures es desglossen en peticions múltiples per als servidors SFTP que limiten la mida de la transferència

* El Konsole actualitza la posició del cursor per als mètodes d'entrada (com l'IBus o el Fcitx) i ja no falla en tancar a través del menú

* El KMail genera un HTML millor en afegir una signatura HTML als correus

[Notes de llançament de la versió
20.04](https://community.kde.org/Releases/20.04_Release_Notes) &bull; [Pàgina
wiki de baixada de
paquets](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;
[Pàgina d'informació del codi font de la versió
20.04.2](https://kde.org/info/releases-20.04.2) &bull; [Registre complet de
canvis de la versió 20.04](https://kde.org/announcements/changelog-
releases.php?version=20.04.2)
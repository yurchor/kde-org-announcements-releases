---
draft: false
layout: page
publishDate: '2020-09-03T12:00:00+00:00'
summary: "Qu\xE8 ha passat a les aplicacions del KDE aquest mes"
title: "Actualitzaci\xF3 de les aplicacions del KDE de setembre de 2020"
type: announcement
---
Un mes tranquil, abans de la nostra gran conferència Akademy d'aquesta setmana. Uniu-vos [en línia des del divendres 4](https://akademy.kde.org/2020/) per parlar de tot sobre KDE.

# Llançaments nous

## PBI 1.7.8

S'ha actualitza la [Integració del navegador del Plasma](https://blog.broulik.de/2020/08/plasma-browser-integration-1-7-6/), el nostre complement per al Chrome i el Firefox.

{{< youtube WGhrR1HsCOM >}}

Això corregeix el funcionament amb els navegadors Vivaldi i Brave browsers. Corregeix els controls de vídeo amb «iframes». Diversos llocs web estableixen la caràtula de l'àlbum a través de l'API de la Javascript Media Session, però per aquells que no ara s'usa el pòster del reproductor de vídeo com una caràtula d'àlbum. I corregeix la detecció de les notificacions curtes de «missatge nou» que no necessiten controls amb les que informen incorrectament de la seva longitud.

## Kontrast

{{< img class="text-center" src="kontrast.png" caption="Kontrast" style="width: 600px">}}

{{< img class="text-center" src="kontrast2.png" caption="Kontrast" style="width: 600px">}}

El Kontrast és la nova eina d'usabilitat d'aquest mes. És un comprovador del contrast del color i indica si les vostres combinacions de color són accessibles per a persones amb deficiències de visió de color.

## KPhotoAlbum 5.7

{{< img class="text-center" src="kpa-5.7-homescreen-light+dark.png" caption="El KPhotoAlbum clar i fosc" style="width: 600px">}}

El [KPhotoAlbum 5.7](https://www.kphotoalbum.org/2020/08/09/0102/) ha efectuat un llançament nou endreçant el codi però també afegint alguna funcionalitat nova.

S'ha afegit la implementació dels esquemes de color personalitzats. Ara el KPhotoAlbum també té un «Mode fosc». Hi ha opcions experimentals per afinar la cerca d'imatges. I també s'ha afegit l'eina de la línia d'ordres «kpa-thumbnailtool» per a gestionar la memòria cau de les miniatures del KPA.

# Correcció d'errors

## Tellico

El gestor de col·leccions [Tellico](https://tellico-project.org/tellico-3-3-2-released/) ha efectuat una actualització que millora l'accés a la [col·lecció de cinema francès Allocine](http://www.allocine.fr/), a la [col·lecció de cinema rus Kinopoisk](https://www.kinopoisk.ru/), a [Goodreads, el lloc més gran del món per a lectors i recomanacions de llibres](https://www.goodreads.com/) i a la nostra pròpia [KDE Store](https://store.kde.org/browse/cat/591/).

# Entrades

Aquest mes s'ha afegit a KDE Review el connector [SeExpr per al Krita](https://invent.kde.org/graphics/seexpr), una bifurcació del SeExpr dels Walt Disney Animation Studios, per al Google Summer of Code 2020.

# Botigues d'aplicacions

Hi ha una actualització sobre [l'objectiu «KDE is All About the Apps»](https://conf.kde.org/en/akademy2020/public/events/211) a la conferència en línia de l'Akademy, aquest dissabte 5 de setembre a les 09:30 UTC.

Una xerrada lleugera sobre [Flatpak, Flathub i KDE](https://conf.kde.org/en/akademy2020/public/events/264) a les 18:30 UTC del dissabte.

Més tard al [dissabte hi haurà programes d'aprenentatge](https://community.kde.org/Akademy/2020/Monday) sobre l'Snaps, l'empaquetament del Neon, construir el primer «flatpak» i fer l'Appimage d'una aplicació.

# Llançament 20.08.1

Diversos projectes es publiquen segons la seva pròpia escala de temps i altres es publiquen conjuntament. El paquet de projectes 20.08.1 s'ha publicat avui i aviat estarà disponible a través de les botigues d'aplicacions i de les distribucions. Vegeu la [pàgina del llançament 20.08.1](https://www.kde.org/info/releases-20.08.1php) per als detalls.

Algunes de les correccions al llançament d'avui:

* Al Kontact, el codis QR ara funcionen a la vista prèvia dels contactes.

* Ara es torna a aplicar correctament el paràmetre de calibratge de l'espai del disc a la barra d'estat del Dolphin.

* S'ha corregit la creació d'alarmes a partir de plantilles al recordatori d'esdeveniments del KAlarm.

* Els fluxos de només àudio ara es poden inserir a la línia de temps de vídeo del Kdenlive.

[Notes de llançament de la versió
20.08](https://community.kde.org/Releases/20.08_Release_Notes) &bull; [Pàgina
wiki de baixada de
paquets](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;
[Pàgina d'informació del codi font de la versió
20.08.1](https://kde.org/info/releases-20.08.1) &bull; [Registre complet de
canvis de la versió 20.08.01](https://kde.org/announcements/changelog-
releases.php?version=20.08.1)
---
draft: false
layout: page
publishDate: '2020-09-03T12:00:00+00:00'
summary: Wat gebeurde er in de toepassingen van KDE deze maand
title: Update van toepassingen van KDE in september 2020
type: announcement
---
Een rustige maand voor onze grote conferentie dit weekend Akademy. Doe mee [online vanaf vrijdag de 4de](https://akademy.kde.org/2020/) om over alle zaken van KDE te praten.

# Nieuwe uitgaven

## PBI 1.7.8

[Plasma Browser Integration](https://blog.broulik.de/2020/08/plasma-browser-integration-1-7-6/) onze addon voor Chrome en Firefox kreeg een update.

{{< youtube WGhrR1HsCOM >}}

Dit repareert het werken met browsers Vivaldi en Brave. Het repareert videobesturing met iframes. Sommige websites stellen de illustratie op de albumhoes in via de Javascript Media Session API maar voor hen die de poster van de videospeler niet doen wordt nu een albumhoes gebruikt. En het repareert de detectie van korte meldingen "nieuw bericht" die geen besturing nodig hebben met die diegenen die hun lengte verkeerd rapporteren.

## Kontrast

{{< img class="text-center" src="kontrast.png" caption="Kontrast" style="width: 600px">}}

{{< img class="text-center" src="kontrast2.png" caption="Kontrast" style="width: 600px">}}

Een nieuw hulpmiddel voor toegankelijkheid deze maand is Kontrast. Het is een controleprogramma op kleurcontrast en vertelt u of uw kleurcombinaties toegankelijk zijn voor mensen met kleurenblindheid.

## KPhotoAlbum 5.7

{{< img class="text-center" src="kpa-5.7-homescreen-light+dark.png" caption="KPhotoAlbum Licht en Donker" style="width: 600px">}}

[KPhotoAlbum 5.7](https://www.kphotoalbum.org/2020/08/09/0102/) kreeg een nieuwe uitgave met opgeschoonde code maar ook enige nieuwe functies.

Het heeft ondersteuning voor aangepaste kleurschema's toegevoegd. KPhotoAlbum heeft nu ook een “Donkere modus”. Er zijn experimentele opties afregelen voor zoeken naar afbeeldingen. Er is ook een opdrachtregelhulpmiddel kpa-thumbnailtool om de miniaturencache van KPA te beheren toegevoegd.

# Verbeterde bugs

## Tellico

Beheerder van verzamelingen [Tellico](https://tellico-project.org/tellico-3-3-2-released/) werd bijgewerkt wat toegang tot [French cinema collection Allocine](http://www.allocine.fr/), [Russian Cinema collection Kinopoisk](https://www.kinopoisk.ru/), [Goodreads, de werelds grootste site voor aanbevelingen aan lezers en boeken](https://www.goodreads.com/) en onze zeer eigen [KDE Store](https://store.kde.org/browse/cat/591/) verbeterde.

# Inkomend

Aan KDE Review deze maand toegevoegd is de plug-in [SeExpr voor Krita](https://invent.kde.org/graphics/seexpr), een vork van Walt Disney Animation Studios' SeExpr, voor Google Summer of Code 2020.

# App Store

Er is een update op de [KDE is All About the Apps goal at Akademy](https://conf.kde.org/en/akademy2020/public/events/211) online conferentie, deze zaterdag 5 September om 09:30 UTC.

Een verlichtende voordracht over [Flatpak, Flathub en KDE](https://conf.kde.org/en/akademy2020/public/events/264) om 18:30 UTC op zaterdag.

Daarna [zaterdag bevat inleidingen](https://community.kde.org/Akademy/2020/Monday) over Snaps, Neon packaging, Building van uw eerste flatpak en "Make your App an Appimage".

# Uitgaven 20.08.1

Sommigen van onze projecten worden vrijgegeven in hun eigen tijdschaal en sommigen in massa vrijgegeven. De bundel 20.08.1 van projecten is vandaag vrijgegeven en zouden spoedig beschikbaar moeten zijn via opslagruimten voor toepassingen en distributies. Zie de [20.08.1 uitgavepagina](https://www.kde.org/info/releases-20.08.1.php) voor details.

Enige van de reparaties in de uitgave van vandaag:

* In Kontact werken nu QR-codes in de voorbeelden van contactpersonen.

* De instelling voor de maat van schijfruimte in de statusbalk van Dolphin is opnieuw juist toegepast.

* Alarmen uit sjablonen maken in de gebeurtenismelder van KAlarm is gerepareerd.

* Streams met alleen geluid kunnen nu ingevoegd worden in de videotijdlijn van Kdenlive.

[20.08 uitgavenotities](https://community.kde.org/Releases/20.08_Release_Notes)
&bull; [Wiki-pagina voor downloaden van
pakketten](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [20.08.1 informatiepagina van
broncode](https://kde.org/info/releases-20.08.1) &bull; [20.08.1 volledige log
met wijzigingen](https://kde.org/announcements/changelog-
releases.php?version=20.08.1)
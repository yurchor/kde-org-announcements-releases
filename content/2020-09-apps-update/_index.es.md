---
draft: false
layout: page
publishDate: '2020-09-03T12:00:00+00:00'
summary: "\xBFQu\xE9 ha ocurrido este mes en las Aplicaciones de KDE?"
title: "Actualizaci\xF3n de las Aplicaciones de KDE de septiembre de 2020"
type: announcement
---
Un mes tranquilo antes de nuestra gran conferencia Akademy de este fin de semana. Únase a nosotros [en línea desde el viernes 4](https://akademy.kde.org/2020/) para hablar de todo sobre KDE.

# Nuevos lanzamientos

## PBI 1.7.8

Se ha actualizado la [integración del navegador en Plasma](https://blog.broulik.de/2020/08/plasma-browser-integration-1-7-6/), nuestro complemento para Chrome y Firefox.

{{< youtube WGhrR1HsCOM >}}

Esto soluciona el funcionamiento con los navegadores Vivaldi y Brave. También corrige los controles de vídeo con «iframes». Algunos sitios web establecen la portada del álbum usando la API de Javascript Media Session; para los que no lo hacen, el póster del reproductor de vídeo se usa ahora como portada del álbum. Además, se corrige la detección de notificaciones breves de «nuevo mensaje» que no necesitan controles con las que solo informan de su longitud erróneamente.

## Kontrast

{{< img class="text-center" src="kontrast.png" caption="Kontrast" style="width: 600px">}}

{{< img class="text-center" src="kontrast2.png" caption="Kontrast" style="width: 600px">}}

La nueva herramienta de accesibilidad de este mes es Kontrast, un verificador de contraste de color que le dice si sus combinaciones de colores son accesibles para personas con deficiencias de percepción de colores.

## KPhotoAlbum 5.7

{{< img class="text-center" src="kpa-5.7-homescreen-light+dark.png" caption="KPhotoAlbum claro y oscuro" style="width: 600px">}}

[KPhotoAlbum 5.7](https://www.kphotoalbum.org/2020/08/09/0102/) es un nuevo lanzamiento que contiene limpieza del código, aunque también alguna nueva funcionalidad.

Permite el uso de esquemas de color personalizados. KPhotoAlbum también dispone ahora de un «modo oscuro». Contiene opciones experimentales para afinar la búsqueda de imágenes. También añade una herramienta para la línea de órdenes, «kpa-thumbnailtool», para gestionar la caché de miniaturas del programa.

# Corrección de errores

## Tellico

El gestor de colecciones [Tellico](https://tellico-project.org/tellico-3-3-2-released/) ha obtenido una actualización que mejora el acceso a la [colección de cine francesa Allocine](http://www.allocine.fr/), a la [colección de cine rusa Kinopoisk](https://www.kinopoisk.ru/), a [Goodreads, el sitio más grande del mundo para lectores y recomendaciones de libros](https://www.goodreads.com/) y a nuestra propia [KDE Store](https://store.kde.org/browse/cat/591/).

# Novedades

Este mes se ha añadido a la revisión de KDE el complemento [SeExpr para Krita](https://invent.kde.org/graphics/seexpr), un «fork» de SeExpr, de los Estudios de Animación Walt Disney, como trabajo de Google Summer of Code 2020.

# Tienda de aplicaciones

Existe una actualización sobre la conferencia en línea [el objetivo KDE es todo sobre las aplicaciones en la Akademy](https://conf.kde.org/en/akademy2020/public/events/211), este sábado 5 de septiembre a las 09:30 UTC.

Una charla relámpago sobre [Flatpak, Flathub y KDE](https://conf.kde.org/en/akademy2020/public/events/264) el sábado a las 18:30 UTC.

Luego, el [sábado tendrá tutoriales](https://community.kde.org/Akademy/2020/Monday) sobre Snaps, empaquetado para Neon, cómo crear su primer flatpak y cómo convertir su aplicación en una Appimage.

# Lanzamiento de 20.08.1

Algunos de nuestros proyectos se publican según su propio calendario, mientras que otros se publican en masa. El paquete de proyectos 20.08.1 se ha publicado hoy y debería estar disponible próximamente en las tiendas de aplicaciones y en las distribuciones. Consulte la [página de lanzamientos 20.08.1](https://www.kde.org/info/releases-20.08.1) para más detalles.

Algunas de las correcciones de los lanzamientos de hoy:

* En Kontact, los códigos QR ya funcionan en la vista previa de los contactos.

* La preferencia para el indicador de espacio libre del disco en la barra de estado de Dolphin se vuelve a aplicar correctamente.

* Se ha corregido la creación de alarmas a partir de plantillas en el recordatorio de eventos de KAlarm.

* Ya se pueden insertar los flujos de solo audio en la línea de tiempo de vídeo de Kdenlive.

[Notas del lanzamiento de
20.08](https://community.kde.org/Releases/20.08_Release_Notes) &bull; [Página
wiki de descarga de
paquetes](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [Página de información principal sobre
20.08.1](https://kde.org/info/releases-20.08.1) &bull; [Registro completo de
cambios de 20.08.1](https://kde.org/announcements/changelog-
releases.php?version=20.08.1)
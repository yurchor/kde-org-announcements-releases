---
draft: false
layout: page
publishDate: '2020-09-03T12:00:00+00:00'
summary: "Vad har h\xE4nt i KDE:s program den h\xE4r m\xE5naden"
title: KDE:s programuppdateringar i september 2020
type: announcement
---
En lugn månad innan vår stora konferens Akademy det här veckoslutet. Gå med på nätet från fredag den 4:e](https://akademy.kde.org/2020/) för att prata om allt som rör KDE.

# Nya utgåvor

## PBI 1.7.8

[Plasma Browser Integration](https://blog.broulik.de/2020/08/plasma-browser-integration-1-7-6/) våra tillägg för Chrome och Firefox har uppdaterats.

{{< youtube WGhrR1HsCOM >}}

Rättar funktionen med webbläsarna Vivaldi och Brave. Rättar videokontroller med iframes. Vissa webbplatser anger albumomslag via Javascript  Media Session programmeringsgränssnittet, men för de som inte gör det används nu videospelarens affisch som ett albumomslag. Och det rättar detekteringen av korta "new message" underrättelser som inte behöver kontroller med de som bara rapporterar sin längd felaktigt.

## Kontrast

{{< img class="text-center" src="kontrast.png" caption="Kontrast" style="width: 600px">}}

{{< img class="text-center" src="kontrast2.png" caption="Kontrast" style="width: 600px">}}

Ett nytt verktyg för handikappstöd denna månad är Kontrast: Det är ett kontrollverktyg för färgkontrast och talar om för dig om dina färgkombinationer är tillgängliga för personer med färgblindhet.

## KPhotoAlbum 5.7

{{< img class="text-center" src="kpa-5.7-homescreen-light+dark.png" caption="Kfotoalbum ljus och mörk" style="width: 600px">}}

[KPhotoAlbum 5.7](https://www.kphotoalbum.org/2020/08/09/0102/) har en ny utgåva med kodförbättringar men också några nya funktioner.

Stöd för egna färgscheman har lagts till. Kfotoalbum har nu också ett "mörkt läge". Det finns ett experimentellt alternativ för finjustering av bildsökning. Och ett kommandoradsverktyg (kpa-thumbnailtool) har också lagts till för att hantera KPA:s miniatyrbildscache.

# Felrättningar

## Tellico

Samlingshanteraren [Tellico](https://tellico-project.org/tellico-3-3-2-released/) har en uppdatering som förbättrar åtkomst till den [franska filmsamlingen Allocine](http://www.allocine.fr/), [ryska filmsamlingen Kinopoisk](https://www.kinopoisk.ru/), [Goodreads, världens största plats för läsare och bokrekommendationer](https://www.goodreads.com/) och vår alldeles egna [KDE-butik](https://store.kde.org/browse/cat/591/).

# Inkommande

Tillägg i KDE Review denna månad är insticksprogrammet [SeExpr för Krita](https://invent.kde.org/graphics/seexpr), en gren av Walt Disney Animation Studios' SeExpr, för Google Summer of Code 2020.

# Programbutik

Det finns en uppdatering om målet [KDE handlar om programmen på Akademy](https://conf.kde.org/en/akademy2020/public/events/211) nätkonferens, lördag den 5 september 09:30 UTC.

Ett snabbföredrag om  [Flatpak, Flathub och KDE](https://conf.kde.org/en/akademy2020/public/events/264) 18:30 UTC på lördag.

Sedan [har lördag handledningar](https://community.kde.org/Akademy/2020/Monday) om Snaps, paketering med Neon, bygga din första flatpak, och göra ditt program till en Appimage.

# Utgåvor 20.08.1

Vissa av våra projekt ges ut med sin egen tidsskala och vissa ges ut tillsammans. Packen med projekt 20.04.2 gavs ut idag och kommer snart vara tillgänglig via programvarubutiker och distributioner. Se [20.08.1 utgivningssida](https://www.kde.org/info/releases-20.08.1.php) för detaljerad information.

Några av felrättningarna som ingår i dagens utgåvor:

* I Kontact, fungerar nu QR-koder i förhandsgranskningen av kontakter.

* Inställningen av diskutrymmesmätaren i Dolphins statusrad appliceras åter korrekt.

* Att skapa alarm från mallar i Kalarms händelsepåminnelse har rättats.

* Strömmar med enbart ljud kan nu infogas i Kdenlives videotidslinje.

[20.08 versionsfakta](https://community.kde.org/Releases/20.08_Release_Notes)
&bull; [Wikisida för paketnerladdning](https://community.kde.org/Get_KDE_Softwar
e_on_Your_Linux_Distro) &bull; [20.08.1
källkodsinformationssida](https://kde.org/info/releases-20.08.1) &bull; [20.08.1
fullständig ändringslogg](https://kde.org/announcements/changelog-
releases.php?version=20.08.1)
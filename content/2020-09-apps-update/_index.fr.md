---
draft: false
layout: page
publishDate: '2020-09-03T12:00:00+00:00'
summary: Quoi de neuf ce mois-ci avec les applications de KDE
title: "Mise \xE0 jour des applications de KDE de Septembre 2020"
type: announcement
---
Un mois tranquille avant notre grande conférence Akademy, cette fin de semaine. Rejoignez nous  [en ligne à partir de Vendredi 04](https://akademy.kde.org/2020/) pour parler de tout sorte de choses autour de KDE.

# Nouvelles mises à jour

## PBI 1.7.8

[Intégration du navigateur de Plasma](https://blog.broulik.de/2020/08/plasma-browser-integration-1-7-6/) : notre module externe pour Chrome et Firefox a reçu une mise à jour.

{{< youtube WGhrR1HsCOM >}}

This fixes it working with Vivaldi and Brave browsers. It fixes video controls with iframes. Some websites set the album cover art through the Javascript Media Session API but for those which don't the video player’s poster is now used as an album cover. And it fixes the detection of short "new message" notifications that do not need controls with those which just report their length wrongly.

## Kontrast

{{< img class="text-center" src="kontrast.png" caption="Kontrast" style="width: 600px">}}

{{< img class="text-center" src="kontrast2.png" caption="Kontrast" style="width: 600px">}}

Un nouvel outil d'accessibilité est arrivé ce mois-ci, Kontrast. Ce dernier est un vérificateur de contraste de couleurs. Il vous indique si les combinaisons de couleurs sont accessibles pour les personnes souffrant de déficiences visuelles.

## KPhotoAlbum 5.7

{{< img class="text-center" src="kpa-5.7-homescreen-light+dark.png" caption="KPhotoAlbum clair et sombre" style="width: 600px">}}

[KPhotoAlbum 5.7](https://www.kphotoalbum.org/2020/08/09/0102/) a publié une nouvelle version avec un code nettoyé et aussi quelques nouvelles fonctionnalités.

Il a pris en charge des thèmes personnalisés de couleurs. KPhotoAlbum a aussi maintenant son « mode sombre ». Il y a des options expérimentales pour les réglages de recherche d'images. Enfin, un outil en ligne de commandes « kpa-thumbnailtool » a été ajouté pour gérer le cache de vignettes de « KPA ».

# Corrections de bogues

## Tellico

Le gestionnaire de collections [Tellico](https://tellico-project.org/tellico-3-3-2-released/) a publié une mise à jour améliorant l'accès aux [collections de cinéma français de « Allocine »](http://www.allocine.fr/), [collections de cinéma russe de « Kinopoisk »](https://www.kinopoisk.ru/), [le site « Goodreads », le plus grand site du monde pour les lecteurs et les recommandations de livres](https://www.goodreads.com/) 

# Entrant

Le module [« SeExpr » pour Krita](https://invent.kde.org/graphics/seexpr) a été ajouté ce mois aux revues de KDE. Ce module développé durant le « Google Summer of Code 2020 » est une implémentation libre de « SeExpr » du studio d'animation Walt Disney.

# Magasin d'applications

Il y a une mise à jour pour [l'objectif « KDE fait tout pour les applications » ](https://conf.kde.org/en/akademy2020/public/events/211)de la conférence en ligne Akademy, ce Samedi 05 Septembre à 09:30 (UTC).

Une discussion rapide sur [Flatpak, Flathub and KDE](https://conf.kde.org/en/akademy2020/public/events/264) à 18:30 (UTC), le Samedi.

Ensuite, [des tutoriels seront présentés Samedi](https://community.kde.org//Akademy/2020/Monday) sur les paquets « snap », la mise en paquet sous Neon, la création de votre premier paquet « flatpak » et la mise en paquet de votre application avec « Appimage ».

# Mises à jour 20.08.1

Les mises à jour de certains projets se font selon leurs propres plannings et quelques uns sont mis à jour de façon synchronisée. Le groupe de projets pour la version 20.08.1 a publié leurs mises à jour aujourd'hui. Celles-ci devraient être disponibles très bientôt dans les dépôts d'applications et dans les distributions. Veuillez consulter [la page des mises à jour 20.08.1](https://www.kde.org/info/releases-20.08.1) pour plus de détails.

Certaines de ces corrections de la mise à jour de ce jour :

* Dans Kontact, les codes « QR » fonctionnent maintenant dans les vignettes de contacts.

* Les paramètres pour la jauge d'espace disque dans la barre d'état de Dolphin sont de nouveau pris en compte correctement.

* La création d'alarmes à partir de modèle dans les rappels d'évènements de KAlarm a été corrigée.

* Les flux uniquement audio peuvent maintenant être insérés dans la frise temporelle vidéo de Kdenlive.

[Notes de la version
20.08](https://community.kde.org/Releases/20.08_Release_Notes) • [Page de
tutoriel pour le téléchargement de
paquets](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) •
[Page d'informations sur les sources de la version
20.08.1](https://kde.org/info/releases-20.08.1) • [Liste complète des
modifications pour la version 20.08.1](https://kde.org/announcements/changelog-
releases.php?version=20.08.1)
---
title: KDE's MONTH 2020 Apps Update

publishDate: 20yy-xx-00 16:00:00

layout: page # don't translate

summary: "What Happened in KDE's Applications This Month"

type: announcement # don't translate
draft: true
---

On repo-metadata:
git diff 'HEAD@{1 month ago}' HEAD

ssh ftpadmin@master.kde.org 'find stable/ -mtime -31'

https://mail.kde.org/pipermail/kde-announce-apps/

https://mail.kde.org/pipermail/kde-announce/

Intro Paragraph

# New releases

## Foo 1.0

Text

{{< img class="text-center" src="foo.png" caption="Foo" style="width: 600px">}}

To find out more about Foo, [My Link Text](https://mylink.com/) and a video on Kup not long ago:

{{< youtube y53IE0G8Brg >}}

# Bugfixes

# Incoming

Stuff that passed incubator or kdereview or got a beta release or newly created projects.

# App Store

App store of the month review, Snap, F-Droid, Google Play, Microsoft, Mac App Store, Flathub, Appimagehub etc

# Releases 20.yy.z

Some of our projects release on their own timescale and some get released en-masse. The 20.yy.z bundle of projects was released today and will be available through app stores and distros soon. See the [20.yy.z releases page](https://www.kde.org/info/releases-20.yy.z) for details.

Some of the fixes in today's releases:

 * foo
 * bar
 * baz

[20.xx release notes](https://community.kde.org/Releases/20.xx_Release_Notes) &bull; [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;  [20.yy.z source info page](https://kde.org/info/releases-20.yy.z) &bull; [20.yy.z full changelog](https://kde.org/announcements/changelog-releases.php?version=20.yy.z)

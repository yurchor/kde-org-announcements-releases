---
layout: page
publishDate: 2020-07-09 12:00:00
summary: "Qu\xE8 ha passat a les aplicacions del KDE aquest mes"
title: "Actualitzaci\xF3 de les aplicacions del KDE de juliol de 2020"
type: announcement
---
# Llançaments nous

### KTorrent 5.2.0

L'aplicació de compartició de fitxers [KTorrent](https://kde.org/applications/ca/internet/org.kde.ktorrent) ha efectuat el llançament nou [5.2.0](https://mail.kde.org/pipermail/kde-announce-apps/2020-June/005591.html).

El més destacable per a compartir són les millores a la [Distributed Hash Table] (https://en.wikipedia.org/wiki/Mainline_DHT) que ara arrenca amb nodes ben coneguts, que permeten aconseguir més ràpidament les baixades. Sota el capó, s'ha actualitzat al QtWebengine més recent, el qual està basat en el Chrome i deixa enrere l'antic QtWebkit basat en el WebKit (tots ells basats en el KHTML del KDE de fa temps).

{{< img class="text-center" src="ktorrent.png" caption="KTorrent" style="width: 600px">}}

El KTorrent està disponible a la vostra distribució de Linux.

### S'ha publicat el KMyMoney 5.1.0

L'aplicació de banca en línia [KMyMoney](https://kmymoney.org/) [ha publicat la versió 5.1](https://blog.bembel.net/2020/06/kmymoney-5-1-0-released/).

Afegeix el suport per al símbol de la rúpia índia: ₹. També s'ha afegit l'opció «Inverteix els càrrecs i els pagament» dels imports OFX i la vista de pressupost ara mostra tots els tipus de comptes.

{{< img class="text-center" src="kmymoney.png" caption="KMyMoney" style="width: 600px">}}

El [KMyMoney està disponible](https://kmymoney.org/download.html) a la vostra distribució de Linux, com una baixada per a Windows, com una baixada per a Mac, i ara està a [Homebrew KDE](https://invent.kde.org/packaging/homebrew-kde).

### Notes de llançament del KDiff3 1.8.3

L'eina de comparació de fitxers [KDiff3](https://kde.org/applications/ca/development/org.kde.kdiff3) ha publicat la nova versió 1.8.3 que s'ha llançat amb una pila de correccions d'estabilitat.

Ja no es tornaran a activar errors en fitxers no existents en usar el KDiff3 com a eina de diferències per al Git. Els errors durant la comparació de directoris es posen en cua adequadament, de manera que només apareixerà un missatge. Corregeix la recàrrega al Windows. S'ha eliminat una fallada quan no està disponible el porta-retalls. La commutació a pantalla completa s'ha tornat a refer per a evitar una crida problemàtica a l'API de les Qt.

Podeu [baixar el KDiff3](https://download.kde.org/stable/kdiff3/) per a Windows, el Mac i la vostra distribució Linux.

# Botigues d'aplicacions

### Estadístiques de la Microsoft Store

[Christoph Cullmann](https://kate-editor.org/post/2020/2020-06-20-windows-store-monthly-statistics/) ens ha donat novetats quant a la Microsoft Store. El Kate i l'Okular s'han actualitzat i en el darrer mes han tingut unes 4000 instal·lacions.

{{< img class="text-center" src="kate-ms-store.png" caption="Kate" style="width: 600px">}}

# Entrevista de la botiga d'aplicacions: Flathub

El Flatpak és un dels formats nous basats en contenidors que han canviat la manera d'obtenir aplicacions al Linux. El Flatpak pot funcionar amb qualsevol amfitrió que vulgui establir una botiga, però la botiga definitiva és [Flathub](https://flathub.org/home).

Recentment, el col·laborador del Flathub [Timothée Ravier](https://discourse.flathub.org/u/Siosm) [ha demanat ajuda per a posar més aplicacions KDE a la botiga](https://discourse.flathub.org/t/kde-apps-call-for-contributors/514). L'hem entrevistat per a conèixer-lo més.

### Parleu-nos sobre vós mateix, d'on veniu, què feu per a viure, com heu arribat al codi font obert i als Flatpak?

El meu nom és Timothée Ravier i actualment estic vivint a París, França. Sóc enginyer de sistemes Linux i actualment treballo a Red Hat al Red Hat CoreOS i al Fedora CoreOS.

Vaig arribar al codi font obert quan vaig instal·lar per primera vegada una distribució de Linux el 2006, i des de llavors no he parat. La majoria dels projectes de recerca que vaig emprendre durant els meus estudis estaven relacionats amb la seguretat a Linux, aplicacions en entorns protegits i seguretat de la interfície gràfica. Per tant, la introducció i el desenvolupament del Flatpak van centrar el meu interès.

En el meu temps lliure mantinc la variant no oficial del [KDE (amb sobrenom Kinoite)] (https://discussion.fedoraproject.org/t/kinoite-a-kde-and-now-xfce-version-of-fedora-silverblue/147) del Fedora Silverblue. Ras i curt, el Fedora Silverblue és un sistema operatiu immutable i la manera recomanada d'instal·lar-hi aplicacions és usar Flatpaks o contenidors (a través del «podman»). Vegeu la documentació per a més detalls.

### Què ha fet que que genereu la crida recent de les aplicacions KDE al Flathub?

Primer, vull donar moltes gràcies als mantenidors actuals que ja mantenen aplicacions KDE al Flathub perquè estan fent un gran treball!

He estat un usuari del KDE des de fa temps (vaig començar el 2006) i sempre he volgut retornar la col·laboració. Les distribucions ja tenen equips establerts de mantenidors i el Flathub estava mancat d'una bona pila de les aplicacions KDE, així que vaig veure que era un bon lloc per a començar.

També he fet una crida ja que serà més fàcil si dividim el treball i potser això també fa que hi hagi més gent conscient dels Flatpaks i el Flathub.

### El Flatpak pot funcionar des de qualsevol repositori, perquè cal Flathub?

Aquesta qüestió ressalta un dels avantatges de Flathub: podeu hostatjar el vostre propi repositori d'aplicacions al vostre servidor i distribuir-les directament als usuaris. No «cal» Flathub.

Però de la mateixa manera no cal GitHub o GitLab, etc. per a hostatjar un repositori Git, és molt més fàcil col·laborar si hi ha un únic lloc al qual dirigir els usuaris i els desenvolupadors.

Flathub ha esdevingut el lloc més fàcil per a trobar i provar amb seguretat les aplicacions Linux, siguin de codi obert lliure o propietari. Penso que és critic si volem millorar l'atractiu de l'ecosistema Linux com a plataforma d'escriptori.

### Quines altres comunitats de codi font obert han acceptat posar les seves aplicacions a Flathub?

Crec que moltes (potser la majoria) de les aplicacions GNOME ara estan disponibles a Flathub.

### Ara que els desenvolupadors d'aplicacions poden posar el programari directament a les botigues com Flathub hi ha responsabilitats noves com la seguretat i mantenir el programari al dia. Podríeu dir-nos com es gestiona això a Flathub?

A Flathub, les responsabilitats són compartides entre els mantenidors de la plataforma i els mantenidors de les aplicacions.

Les plataformes contenen les biblioteques comunes a moltes aplicacions (hi ha plataformes Freedesktop, GNOME i KDE) i es mantenen per a conservar tant la compatibilitat ABI com assegurar les actualitzacions immediates de seguretat.

Les actualitzacions de les biblioteques restants requerides per una aplicació i la mateixa aplicació són responsabilitat del mantenidor de l'aplicació.

### Quina de les aplicacions KDE trobeu que és més útil?

Diàriament uso el Dolphin, Konsole, Yakuake, Okular i Ark, i realment m'agraden. També valoro i uso de tant en tant el Gwenview, KCachegrind i Massif-Visualizer.

### Moltes de les nostres aplicacions s'empaqueten com a Flatpaks a través del nostre [invent](https://invent.kde.org/packaging/flatpak-kde-applications) i dels [servidors «binary-factory»](https://binary-factory.kde.org/view/Flatpak/). Esteu treballant amb aquests processos o separadament?

Els Flatpaks que es construeixen a la infraestructura del KDE estan destinats a ser construccions nocturnes per a proves dels desenvolupadors i usuaris. Aquesta és una font bona d'aplicacions Flatpak per a començar, però cal actualitzar alguna d'elles. Mantenir aquest repositori actualitzat ens ajudarà amb els desenvolupaments recents que poden requerir canvis a l'empaquetatge al Flathub. Encara no he començat a actualitzar-les, però intentaré fer-ho amb l'entrada d'aplicacions a Flathub.

### Veurem alguna vegada la desaparició dels RPM i l'Apt, i que totes les distribucions Linux usin paquets contenidors?

No penso que passi mai ja que hi ha molt valor en com les distribucions empaqueten actualment les aplicacions, tot i que hi haguin problemes. Però crec que menys distribucions faran aquest esforç. Per exemple, Fedora construeix Flatpaks a partir de paquets RPM i els fa disponibles a tothom. Potencialment també es pot fer el mateix amb els paquets Debian. Aquí el valor no és què sinó qui: confieu en aquesta distribució? Els seus valors? Els seu compromís només amb el programari lliure? Llavors estareu segur que les aplicacions que instal·leu del seu repositori tindran els mateixos requirements que qualsevol altre paquet. Flathub té tant aplicacions de codi fon obert com propietari i això pot no ser per a tothom.

# Ara es publiquen a kde.org/applications

El nostre [sublloc d'aplicacions](https://kde.org/applications) ha començat a mostrar la informació de les publicacions. Espereu-ne més properament. Si sou un mantenidor d'aplicacions recordeu afegir la informació de publicació als fitxers Appstream.

{{< img class="text-center" src="krita-release.png" caption="Informació del llançament" style="width: 800px">}}

# Llançament 20.04.3

Diversos projectes publiquen en la seva pròpia escala de temps i altres es publiquen conjuntament. El paquet de projectes 20.04.3 s'ha publicat avui i aviat estarà disponible a través de les botigues d'aplicacions i de les distribucions. Vegeu la [pàgina del llançament 20.04.3](https://kde.org/info/releases-20.04.3) per als detalls.

Algunes de les correccions al llançament d'avui:

* S'han corregit les vistes prèvies dels fitxers «desktop» al Dolphin per als camins absoluts de les icones

* Els elements de tasques pendents completades ara s'enregistren correctament al diari del KOrganizer

* El text multilínia enganxat des d'aplicacions GTK al Konsole ja no té caràcters extres de «línia nova»

* S'ha corregit el comportament de maximització del Yakuake

[Notes de llançament de la versió
20.04](https://community.kde.org/Releases/20.04_Release_Notes) &bull; [Pàgina
wiki de baixada de
paquets](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;
[Pàgina d'informació del codi font de la versió
20.04.3](https://kde.org/info/releases-20.04.3) &bull; [Registre complet de
canvis de la versió 20.04](https://kde.org/announcements/changelog-
releases.php?version=20.04.3)
---
layout: page
publishDate: 2020-07-09 12:00:00
summary: "O que se Passou nas Aplica\xE7\xF5es do KDE Neste M\xEAs"
title: "Actualiza\xE7\xE3o de Julho 2020 das Aplica\xE7\xF5es do KDE"
type: announcement
---
# Novos lançamentos

### KTorrent 5.2.0

A aplicação de partilha de ficheiros [KTorrent](https://kde.org/applications/en/internet/org.kde.ktorrent) teve uma nova versão [5.2.0](https://mail.kde.org/pipermail/kde-announce-apps/2020-June/005591.html).

A melhoria da importação para fins de partilha teve melhorias na [Distributed Hash Table](https://en.wikipedia.org/wiki/Mainline_DHT) que agora possibilitam o arranque com nós conhecidos, para que consiga fazer as transferências mais rapidamente. Em segundo plano, ele actualiza para o QtWebengine mais recente, que se baseia no Chrome, e não mais o antigo QtWebkit que se baseava no WebKit (todos eles basearam-se no KHTML do KDE já há muito tempo).

{{< img class="text-center" src="ktorrent.png" caption="KTorrent" style="width: 600px">}}

O KTorrent está disponível na sua distribuição de Linux.

### O KMyMoney 5.1.0 foi lançado

A aplicação de banca [KMyMoney](https://kmymoney.org/) [versão 5.1](https://blog.bembel.net/2020/06/kmymoney-5-1-0-released/).

Adiciona o suporte para o símbolo da Rupia Indiana: ₹.  Também adicionaram a opção para as "Cobranças e pagamentos inversos" na importação OFX; para além disso, a área do orçamento agora mostra todos os tipos de contas.

{{< img class="text-center" src="kmymoney.png" caption="KMyMoney" style="width: 600px">}}

[O KMyMoney está disponível](https://kmymoney.org/download.html) na sua distribuição de Linux, como um pacote para Windows, outro para Mac e agora encontra-se no [Homebrew do KDE](https://invent.kde.org/packaging/homebrew-kde).

### Notas de Lançamento do KDiff3 1.8.3

A ferramenta de comparação de ficheiros [KDiff3](https://kde.org/applications/en/development/org.kde.kdiff3) lançou uma nova versão 1.8.3 com um conjunto de correcções de estabilidade.

A utilização do KDiff3 como uma ferramenta de visualização de diferenças para o Git não irá mais desencadear erros no caso de ficheiros não existentes. Os erros durante a comparação de pastas são devidamente encaminhados, pelo que só irá aparecer uma mensagem. Existem correcções no recarregamento no Windows. Foi removido um estoiro quando a área de transferência não está disponível. A opção para activar/desactivar o ecrã completo foi modificada para evitar uma chamada problemática à API do Qt.

Poderá [obter o KDiff3](https://download.kde.org/stable/kdiff3/) para o Windows, Mac e a sua distribuição de Linux.

# Loja de Aplicações

### Estatísticas da Loja da Microsoft

O [Christoph Cullmann](https://kate-editor.org/post/2020/2020-06-20-windows-store-monthly-statistics/) forneceu-nos uma actualização na Loja da Microsoft. O Kate e o Okular foram actualizados e, no último mês, já tiveram mais de 4000 instalações.

{{< img class="text-center" src="kate-ms-store.png" caption="Kate" style="width: 600px">}}

# Entrevista da Loja de Aplicações: Flathub

O Flatpak é um dos novos formatos baseados em contentores que mudam a forma como obtemos as nossas aplicações no Linux. O Flatpak consegue lidar com qualquer anfitrião que deseje configurar uma loja mas a loja oficial é o [Flathub](https://flathub.org/home).

Recentemente o colaborador do Flathub [Timothée Ravier](https://discourse.flathub.org/u/Siosm) [pediu ajuda para colocar mais aplicações do KDE na loja](https://discourse.flathub.org/t/kde-apps-call-for-contributors/514). Fizemos-lhe uma entrevista para saber um pouco mais.

### Fale-nos sobre si mesmo, de onde vem, o que faz na vida e como é que entrou no mundo 'open source' e nos Flatpaks?

Chamo-me Timothée Ravier e vivo neste momento em Paris, França. Sou um engenheiro de sistemas Linux e trabalho de momento na Red Hat, tanto no Red Hat CoreOS como no Fedora CoreOS.

Entrei no mundo 'open source' quando instalei pela primeira vez uma distribuição de Linux em 2006 e depois nunca mais parei. A maior parte dos projectos de investigação que efectuei durante os meus estudos estavam relacionados com a segurança do Linux, o isolamento de aplicações e a segurança em interfaces gráficas. Como tal, a introdução e desenvolvimento do Flatpak captou o meu interesse.

No meu tempo livre mantenho a variante [não-oficial do KDE (com o nome Kinoite)](https://discussion.fedoraproject.org/t/kinoite-a-kde-and-now-xfce-version-of-fedora-silverblue/147) do Fedora Silverblue. Em resumo, o Fedora Silverblue é um sistema operativo imutável e a forma recomendada de instalar aplicações é usar os Flatpaks ou contentores (através do 'podman'). Veja mais detalhes na documentação.

### O que o fez submeter o seu pedido recente por aplicações do KDE no Flathub?

Em primeiro lugar quero deixar um grande "Obrigado" aos responsáveis de manutenção actuais que já fazem a manutenção das Aplicações do KDE no Flathub, dado que estão a fazer um óptimo trabalho!

Tenho sido um utilizador do KDE já há bastante tempo (comecei em 2006) e sempre quis contribuir com algo de volta. As distribuições já têm equipas estabelecidas com responsáveis de manutenção e o Flathub tinha uma grande falta de Aplicações do KDE, pelo que pareceu-me um bom ponto de partida.

Também fiz esse pedido, já que seria mais fácil se repartíssemos trabalho e também porque ajudaria a pôr as pessoas mais a par dos Flatpaks e do Flathub.

### O Flatpak consegue funcionar a partir de qualquer repositório; qual é a necessidade do Flathub?

Esta pergunta realça uma das vantagens do Flathub: poderá alojar o seu próprio repositório de aplicações no seu próprio servidor e distribuí-las directamente para os seus utilizadores. Você não “precisa” do Flathub.

Por outro lado, assim como não precisa do GitHub ou do GitLab, etc. para alojar um repositório de Git, é muito mais fácil colaborar se tiver um único ponto para indicar às pessoas e aos programadores.

O Flathub tornou-se o local mais simples para procurar e experimentar de forma segura as aplicações de Linux, tanto as de código aberto como as proprietárias. Penso que isso é crítico caso se pretenda melhorar a atracção pelo ecossistema do Linux como ambiente de trabalho gráfico.

### Que outras comunidades de código aberto aceitaram de bom grado colocar as suas aplicações no Flathub?

Penso que boa parte (se calhar a maioria) das aplicações do GNOME estão agora disponíveis no FlatHub.

### Agora que os programadores das aplicações conseguem colocar o nosso 'software' directamente nas lojas como o Flathub, existem novas responsabilidades como a segurança e a manutenção das aplicações actualizadas. Consegue-nos dizer quão bem tratadas são as mesmas no Flathub?

Com o Flathub, as responsabilidades são partilhadas entre os responsáveis de manutenção da Plataforma e os responsáveis da aplicação.

As Plataformas contêm as bibliotecas de base comuns a boa parte das aplicações (estas são as plataformas do Freedesktop, GNOME e KDE) e são mantidas para manter tanto a compatibilidade de ABI como garantir actualizações de segurança imediatas.

As actualizações às restantes bibliotecas necessárias por uma dada aplicação e a própria aplicação são da responsabilidade do responsável de manutenção da aplicação.

### Quais as aplicações do KDE que considera mais úteis?

Eu uso o Dolphin, o Konsole, o Yakuake, o Okular e o Ark diariamente e gosto realmente delas. Também aprecio e uso o Gwenview, o KCachegrind e o Visualizador Massif de vez em quando.

### Muitas das nossas aplicações têm os seus pacotes Flatpak através dos nossos servidores do [Invent](https://invent.kde.org/packaging/flatpak-kde-applications) e [binary-factory](https://binary-factory.kde.org/view/Flatpak/). Está a trabalhar com estes processos ou em separado?

Os Flatpaks que são compilados na infra-estrutura do KDE são normalmente versões compiladas à noite para os programadores e utilizadores experimentarem. Aqui existe um bom conjunto de aplicações Flatpak para começar, mas algumas delas precisam de ser actualizadas. Manter este repositório actualizado ajudar-nos-á com os desenvolvimentos recentes que poderão exigir mudanças nos pacotes no Flathub. Ainda não iniciei a actualização das mesmas, mas tentarei fazê-lo em conjunto com as submissões de aplicações para o Flathub.

### Consegue ver uma altura em que não existam mais RPM's e APT's e que as distribuições de Linux usem todas pacotes de contentores?

Não penso que isso alguma vez irá acontecer, dado que existe valor na forma como as distribuições criam pacotes das aplicações, embora isso também traga problemas. Porém, penso que menos distribuições irão depositar esforço para o fazer. Por exemplo, o Fedora cria Flatpaks a partir dos pacotes RPM e disponibiliza-os para todos. Também poderá potencialmente fazer o mesmo com os pacotes da Debian. O valor aqui não está no quê, mas em quem: confia nessa distribuição? Nos seus valores? No seu compromisso apenas com 'software' livre? Aí sim terá a certeza que as aplicações que instalar a partir dos repositórios deles terão os mesmos requisitos que todos os outros pacotes. O Flathub tem tanto aplicações livres como proprietárias, e isso poderá não ser adequado para todos.

# Versões Agora no kde.org/applications

A nossa [sub-página de Aplicações](https://kde.org/applications) começou a apresentar informações de lançamento nela. Esperem mais sobre o assunto em breve. Se for um responsável de manutenção de uma aplicação, lembre-se de adicionar a informação de lançamento nos ficheiros do Appstream.

{{< img class="text-center" src="krita-release.png" caption="Informação de Lançamento" style="width: 800px">}}

# Versão 20.04.3

Alguns dos nossos projectos são lançados de acordo com as suas próprias agendas e alguns são lançados em massa. A versão 20.04.3 dos projectos foi lançada hoje e deverá estar disponível através das lojas de aplicações e distribuições em breve. Consulte mais detalhes na [página da versão 20.04.3](https://www.kde.org/info/releases-20.04.3).

Algumas das correcções incluídas nesta versão de hoje são:

* As antevisões dos ficheiros .desktop no Dolphin foram corrigidas no que respeita a localizações absolutas dos ícones

* Os itens Por-Fazer completos agora são gravados correctamente no diário do KOrganizer

* O texto em várias linhas colado a partir das aplicações do GTK para o Konsole já não têm mais caracteres de "mudança de linha" extra

* O comportamento de maximização do Yakuake foi corrigido

[Notas da versão 20.04](https://community.kde.org/Releases/20.04_Release_Notes)
&bull; [Página de transferências de pacotes na
Wiki](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;
[Página de informação do código do
20.04.3](https://kde.org/info/releases-20.04.3) &bull; [Registo de alterações
completo do 20.04.3](https://kde.org/announcements/changelog-
releases.php?version=20.04.3)
---
title: 19.12 Lançamentos RC

summary: "Mais de 120 programas, dezenas de bibliotecas para programadores e
extensões de funcionalidades são lançadas ao mesmo tempo como parte do
serviço de lançamento da KDE."

publishDate: 2019-11-29 00:01:00 # don't translate

layout: page # don't translate

type: announcement # don't translate
---

29 de novembro de 2019. Mais de 120 programas, dezenas de bibliotecas para
programadores e extensões de funcionalidades são lançadas ao mesmo tempo
como parte do serviço de lançamento da comunidade KDE.

Hoje, todos esses recebem fontes para candidatos a lançamento, o que
significa que estão terminados mas ainda necessitam de testes para correções
de erros.

Os mantenedores de distros e pacotes devem atualizar seus canais de
pré-lançamento para verificar possíveis problemas.

+ [19.12 notas de
lançamento](https://community.kde.org/Releases/19.12_Release_Notes) for
information on tarballs and known issues.  + [Package download wiki
page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)  +
[19.12 RC source info page](https://kde.org/info/applications-19.11.90)

## Contatos de imprensa

Para mais informações, envie um e-mail para nós:
[press@kde.org](mailto:press@kde.org).

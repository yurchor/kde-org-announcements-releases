---
layout: page
publishDate: 2019-11-29 00:01:00
summary: "Mais de 120 programas, dezenas de bibliotecas para programadores e extens\xF5\
  es de funcionalidades s\xE3o lan\xE7adas ao mesmo tempo como parte do servi\xE7\
  o de lan\xE7amento da KDE."
title: "19.12 Lan\xE7amentos RC"
type: announcement
---
29 de novembro de 2019. Mais de 120 programas, dezenas de bibliotecas para programadores e extensões de funcionalidades são lançadas ao mesmo tempo como parte do serviço de lançamento da comunidade KDE.

Hoje, todos esses recebem fontes para candidatos a lançamento, o que significa que estão terminados mas ainda necessitam de testes para correções de erros.

Os mantenedores de distros e pacotes devem atualizar seus canais de pré-lançamento para verificar possíveis problemas.

* [Notas de lançamento do 19.12](https://community.kde.org/Releases/19.12_Release_Notes) para mais informações sobre tarballs e problemas conhecidos.

* [Página da wiki para baixar os pacotes](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)

* [Página com informações do código fonte do 19.12 RC](https://kde.org/info/applications-19.11.90)

## Contatos de imprensa

Para mais informações, envie um e-mail para nós:
[press@kde.org](mailto:press@kde.org).
---
layout: page
publishDate: 2019-11-29 00:01:00
summary: "\xD6ver 120 individuella program samt dussintalet programmeringsbibliotek\
  \ och funktionsinsticksprogram ges samtidigt ut som del av KDE:s utgivningstj\xE4\
  nst."
title: "19.12 RC-utg\xE5vor"
type: announcement
---
29:e november , 2019. Över 120 individuella program samt dussintalet programmeringsbibliotek och funktionsinsticksprogram ges samtidigt ut som del av KDE:s utgivningstjänst.

Idag ges alla ut som utgivningskandidater, vilket betyder att funktionen är komplett men behöver testas för slutliga felrättningar.

Distributioner och applikationsbutikpacketerare bör uppdatera sina förutgåvekanaler för kontrollera om det finns problem.

* [19.12 versionsfakta](https://community.kde.org/Releases/19.12_Release_Notes) för information om tar-arkiv och kända problem.

* [Wiki-sida för paketnedladdning](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)

* [19.12 Informationssida om källkod](https://kde.org/info/applications-19.11.90)

## Presskontakter

För mer information skicka e-post till oss:
[press@kde.org](mailto:press@kde.org).
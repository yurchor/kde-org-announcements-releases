---
draft: false
layout: page
publishDate: '2020-08-13T14:00:00+02:00'
summary: Hilabete honetan KDEren aplikazioekin gertatutakoa
title: KDE aplikazioen 2020ko abuztuko eguneratzea
type: announcement
---
[KDEren hamabinaka aplikazio](https://kde.org/applications/eu), argitalpen berriak lortzen ari dira, KDEren argitalpen zerbitzuaren bidez. Ezaugarri berriek, erabilgarritasunaren hobekuntzek, birdiseinuek eta akatsen zuzenketek, guztiek laguntzen dute zure produktibitatea handitzen eta aplikazio sorta berri hau eraginkorragoa eta atseginagoa egiten.

Ondokoa espero dezakezu:

## [Dolphin](https://kde.org/applications/eu/system/org.kde.dolphin)

{{< img class="text-center" src="dolphin_preview.webp" caption="Dolphin-ek fitxategi mota askoren aurreikuspegiak erakusten ditu." style="width: 800px" >}}

Dolphin KDEko fitxategi-esploratzailea da, eta fitxategiak eta karpetak bilatzen, kopiatzen, eta irekitzen laguntzen dizu. Hori dela eta, bere eginkizun nagusietako bat fitxategi bakoitzak duenaren ideia argi bat ematea da. Koadro-txikiak erakustea funtsezkoa da horretarako, eta Dolphin aspaldi da irudi, bideoklip, baita Blender-ren 3Dko fitxategien aurreikuspegiak erakusteko gai.

Bertsio berri honetan, Dolphin-ek 3D Fabrikazio Formatuko (3MF - 3D Manufacturing Format) fitxategien koadro-txikiak zerrendara gehitzen ditu eta gainera zifratutako fitxategi-sistemetako fitxategien eta karpeten aurreikuspegiak ere ikus ditzakezu, Plasma Kutxa-gotorretakoak adibidez. Hori era seguruan egiten da, cacheratutako koadro-txikiak fitxategi-sisteman bertan biltegiratuz, edo sortu bai, baino haiek inon cacheratuta biltegiratu gabe, beharrezkoa bada. Nolanahi ere, zuk duzu Dolphin-ek zure fitxategien edukia nola eta noiz erakusten duen gaineko erabateko kontrola, modu independentean konfiguratu baitezakezu, bertako zein urruneko fitxategien aurreikuspegiak azaltzeko neurri muga.

Fitxategiak identifikatzeko beste modu bat izenen bidez egitea da. Fitxategi-izena azaltzeko luzeegia denean, garatzaileek izenak laburtzeko jokabidea birfindu dute. Aurreko bertsioetan bezala fitxategi edo karpeta baten izenaren erdiko zatia moztu ordez, bertsio berrian Dolphin-ek bukaera mozten du, eta beti mantentzen du ikusgai fitxategi-luzapena (dagoenean) elipsiaren ondoren. Honek asko errazten du izen luzeko fitxategiak identifikatzea.

Dolphin-ek, orain, ikusten ari zinen kokalekua gogoratu eta lehengoratzen du, baita irekitako fitxak, eta itxi zenuen azkenekoan irekita zenituen ikuspegi zatituak. Ezaugarri hau aktibatuta dago era lehenetsian, baina Dolphin-en «Ezarpenak» leihoko «abioa» orrian desaktibatu daiteke.

Erabilerraztasuna hobetzeari dagokionez, Dolphin-ek betidanik utzi dizu urrutiko partekatzeak muntatu eta esploratzen, bai FTP, SSH, edo beste protokolo batzuen bidez. Baina, orain, Dolphin-ek urrutiko muntaiak eta FUSE motakoak lagunkoiagoak diren izenekin azaltzen ditu, bide-izen osoarekin azaldu ordez. Oraindik hobeto: ISO irudiak ere munta ditzakezu testuinguru menuko elementu berri bat erabiliz. Horrek esan nahi du, gero disko edo USB memoria batean grabatuko duzun fitxategi-sistema, zama-jaitsi, eta esploratu dezakezula.

{{< video src="dolphin_ISO.mp4" caption="Dolphin-ek ISO irudiak muntatzen uzten dizu." style="max-width: 900px" >}}

Dolphin-ek ezaugarri kopuru estonagarria du, baina pluginen bidez are gehiago gehitu ditzakezu. Lehen, «Zerbitzua» menura pluginak gehitzeko (begiratu <I>Ezarpenak</I> > <I>Konfiguratu Dolphin</I> > <I>Zerbitzuak</I>) sarritan script bat exekutatu edo banaketa bateko pakete bat instalatu behar zenuen. Orain, «Lortu gauza berria» leihotik instalatu ditzakezu, eskuz egin beharreko tarteko urratsik gabe. «Zerbitzuak» ezarpen orriko beste aldaketa batzuk, bilatzen duzuna azkar aurkitzeko, goiko aldean, bilaketa eremu berri bat izatea, eta zerbitzuen zerrenda, orain, alfabetikoki sailkatzea dira.

Gauzak alde batetik bestera mugitzea errazagoa da Dolphin berriarekin, izan ere, orain, zatitutako ikuspegi bateko beirate batean hautatutako fitxategiak beste beiratera azkar mugitu edo kopiatzeko ekintza berriak dituzu. Gertatzen ari dena argitzeko, fitxategi bat arrastatzean, kurtsorea esku atxikitzaile batera aldatzen da era lehenetsian, aurreko bertsioetan bezala «kopiatzeko» kurtsorera egin ordez.

Karpetek diskoan hartzen duten lekuaren neurria ere kalkulatu eta azaldu dezakezu «Zehaztasunak» ikuspegia erabiliz, eta Dolphin-en «Informazio» panelak, orain, zakarrontziari buruzko informazio erabilgarria erakusten du. Dolphin-en bilaketa eremutik emaitzen ikuspegira «behera» gezi-tekla erabiliz nabiga daiteke.

Azkenik, ezaugarri berri erabilgarri bat da Dolphin 20.08 menuko «Kopiatu kokalekua» elementu berria...

## [Konsole](https://kde.org/applications/eu/system/org.kde.konsole)

{{< img class="text-center" src="konsolecolors.webp" caption="Konsole-k ezaugarri berri zirraragarri batzuk lortzen ditu." style="width: 800px" >}}

...Konsole KDEren terminal aplikazioan bezalaxe! Horrek esan nahi du Konsole-n erakutsitako elementu batean klik egin dezakezula, azaleratzen den menuan «Kopiatu kokalekua» aukeratu eta ondoren fitxategi edo direktorioaren bide-izen osoa kopiatu eta dokumentu batean, beste terminal batean, edo aplikazio batean itsatsi dezakezula.

Konsole-k beste ezaugarri berri bat ere badu, ikuspegira datozen lerro berrientzako nabarmentze sotil bat azaltzen duena terminaleko irteerakoa azkar goraka kiribiltzen ari denean, eta zure kurtsorea irudi-fitxategien gainetik igarotzean koadro-txiki baten bidez aurreikuspegi bat erakusten du era lehenetsian. Gainera, Konsole-n azpimarratutako fitxategi batean eskuin-klik egiten duzunean, testuinguru-menuak «Ireki honekin» menu bat azaltzen du, fitxategia aukeratzen duzun edozein aplikaziorekin ireki ahal izan dezazun.

Aldi berean hainbat gauza egiteko, Konsole-ren ikuspegia zatitzea eta fitxak gehitzea aspaldiko ezaugarri bat da, baina orain, ikuspegi zatituetako goiburuak ezgaitu daitezke, eta banatzekoaren zabalera handitu daiteke. Gainera, fitxa batean aktibo dagoen prozesua amaitzen den gainbegiratu dezakezu, baita Konsole-ko fitxei koloreak esleitu ere!

{{< img class="text-center" src="konsole-monitor.webp" caption="Begirale prozesu berria Konsole-n" style="width: 500px" >}}

Fitxei dagokienez, Konsole-ko «Uneko fitxa bereizi» lasterbide lehenetsia, Ktrl+Maius+L, kendu egin da. Dirudienez errazegia zen fitxak nahi gabe bereiztea, benetan egin nahi zena, Ktrl+Maius+K , azaldutakoa garbitzea zenean.

Bertsio honetako azken erabilerraztasun hobekuntza, I formako kurtsorea beti neurri berekoa izan ordez, orain, letra-tipoaren neurrira egokitzean datza.

## [Yakuake](https://kde.org/applications/eu/system/org.kde.yakuake)

{{< img class="text-center" src="yakuake.webp" caption="Yakuake-ren ikono berria" style="width: 200px" >}}

Terminalei dagokienez, Yakuake KDEren goitik-behera zabaltzen den terminal emulatzailea da. Zure pantailaren goiko aldetik zabaltzen da F12 sakatzen duzun bakoitzean eta argitalpen honetarako hobekuntza ugari ditu. Adibidez, orain, Konsole-tik datozen teklatuko lasterbide guztiak konfiguratzen uzten dizu, eta sistema-erretiluko elementu berri bat dago, Yakuake martxan dagoela erakusten dizuna. Ezkutatu dezakezu, noski, baina Yakuake aktibo eta prest dagoen ala ez jakitea errazten du. Hari deitzeko modu grafiko bat ere ematen du.

Beste gauza bat da Yakuake Wayland-en hobetu dugula. Leiho nagusia goran dituzun panelen azpian agertzen zen, baina hori, orain, zuzendu egin da.

## [Digikam](https://kde.org/applications/eu/graphics/org.kde.digikam) 7.0.0

DigiKam argazkiak kudeatzeko KDEren aplikazio profesionala da, eta [taldeak bertsio nagusi berri bat kaleratu berri du, 7.0](https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4).

{{< img class="text-center" src="digikam-face.webp" caption="Digikam aurpegi-ezagutzea" style="width: 800px" >}}

DigiKam 7.0.0-ren alderdi nagusietako bat da aurpegi-ezagutzean egin diren hobekuntzak, orain ikaskuntza sakoneko IAko algoritmoak erabiltzen dituena. Hau nola egin den jakiteko, iazko Akademy-ko hitzaldia ikus dezakezu, eta etorkizuneko bertsioetan aurpegi-ezagutze eta antzemate automatikoa gehiago landuko dira.

{{< video src="https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4" caption="Digikam aurpegi-ezagutzea" style="max-width: 800px" controls="true" >}}

[DigiKam-en Flatpak pakete nagusia erabilgarri dago Flathub-en bidez](https://flathub.org/apps/details/org.kde.digikam), baita probak egin nahi dituztenentzako gaueroko eraikin ezegonkor bat ere. DigiKam, Linux-en, AppImages eta zure banaketako gordetegien bidez ere erabilgarri dago. Badaude Windows eta Mac-erako bertsioak ere.

## [Kate](https://kde.org/applications/eu/utilities/org.kde.kate)

Doikuntza pare bat egin dizkiogu, Kate, gure testu editorearen itxurari. «Ireki azken aldikoa» menuak, elkarrizketa-koadroaren bidez Kate-n irekitako dokumentuez gain, orain, komando-lerroaren bidez edo beste sorburu batzuetatik irekitakoak ere azaltzen ditu. Beste aldaketa bat da, Kate-ren fitxa-barraren itxura, orain, koherentea dela KDEren beste aplikazioetako fitxa-barrarekin, eta fitxa berriak eskuinean irekitzen dituela gainontzeko fitxa-barra gehientsuenek bezalaxe.

## [Elisa](https://kde.org/applications/eu/multimedia/org.kde.elisa)

{{< img class="text-center" src="elisa.webp" caption="Elisa mahaigainerako eta mugikorrerako musika jotzaile arin bat da." style="width: 800px" >}}

Elisa, zure mahaigainean eta zure mugikorrean dabilen musika jotzaile arin bat da. Elisak, orain, genero, artista, edo album guztiak alboko-barran, beste elementuen azpian, azaltzen uzten dizu. Jotzeko-zerrendak, unean jotzen ari den abestiaren aurrerapena lerroan bertan azaltzen du, eta goiko barra, orain, leihoaren neurrira eta gailuaren forma proportziora egokitzen da. Horrek esan nahi du ondo ikusten dela «erretratu» orientazioa duen leiho/gailu batean (zure telefonoan adibidez), eta baita zinez txikia izan arte eskala daitekeela.

## [KStars](https://kde.org/applications/eu/education/org.kde.kstars) 3.4.3

{{< img class="text-center" src="kstars-main-screen.webp" caption="KStars astronomia zaleentzako KDEren aplikazioa da." style="width: 800px">}}

Azkenean, KStars astronomia-aplikazioak kalibratze eta fokuratze gaitasun batzuk gehitu ditu [argitalpen berrian](http://knro.blogspot.com/2020/07/kstars-v343-is-released.html). [Teleskopio astronomiko txikiak zehaztasunez fokatzeko gailua da Bahtinov maskara](https://en.wikipedia.org/wiki/Bahtinov_mask). Motorizatutako fokuratzailerik ez duten eta maskararen laguntzaz eskuz zentratu nahi duten erabiltzaileentzat erabilgarria da. Fokatze-moduluan hautatutako Bahtinov maskara-algoritmoarekin irudi bat hartu ondoren, Ekosek haren barruko irudiak eta izarrak aztertuko ditu. Ekosek Bahtinov izarraren eredua ezagutzen badu, izarraren ereduaren gainean lerroak marraztuko ditu, zirkuluetan, erdian, eta jatorri aldaketa batekin, fokua adierazteko.

{{< img class="text-center" src="kstars-focus.webp" caption="KStars-en fokuratzeko algoritmo berria" style="width: 600px">}}

{{< img class="text-center" src="kstars-calibration.webp" caption="KStars kalibratzea" style="width: 600px" >}}

«Kalibratze-diagrama» azpifitxa bat gehitu dugu «Jito-diagrama»ren eskuinean. Barne-gida kalibratzean erregistratutako muntaketa-kokapenak erakusten ditu. Gauzak ondo doazenean, puntuak bi lerrotan erakutsi beharko lituzke, bata bestearekiko angelu zuzenean, muntaia atzera-aurrera RA noranzkoan bultzatzen duenekoa bata, bestea DEC noranzkoan gauza bera egiten duenekoa. Ez da informazio-piloa, baina ikusteko erabilgarria izan daiteke. Bi lerroak 30ºko angeluan badaude, zerbait ez doa ondo kalibratzearekin! Goran simulagailua erabiliz jardunean erakusten duen irudi bat dago.

[KStars zama-jaisteko erabilgarri dago](https://edu.kde.org/kstars/) Android-erako, Snapcraft dendan, Windows-erako, macOS-erako, eta noski, zure Linux banaketaren gordetegietan.

## [KRDC](https://kde.org/applications/eu/internet/org.kde.krdc)

{{< video src="krdc.mp4" caption="Urruneko kurtsorea duen KRDC" style="max-width: 900px" >}}

KRDC-ek beste makina bateko mahaigaineko saioa ikusi eta kontrolatzeko aukera eskaintzen dizu. Gaur argitaratutako bertsioak, VNC-n, zerbitzari-aldeko kurtsore egokiak azaltzen ditu, atzean kurtsorea falta duen puntu txiki baten ordez.

## [Okular](https://kde.org/applications/eu/graphics/org.kde.okular)

Okular KDEren dokumentu erakuslea da. PDF dokumentuak, ePUB liburuak eta testua oinarri duten beste fitxategi askoz gehiago irakurtzeko aukera ematen dizu. Konponketa batek atzera elkarren ondoan ipini ditu «Inprimatu» eta «Inprimatze aurreikuspegia», «Fitxategi» menuan.

## [Gwenview](https://kde.org/applications/eu/graphics/org.kde.gwenview)

Gwenview irudiak ikusteko aplikazio bat da, editatzeko oinarrizko ezaugarri batzuk dituena, neurria aldatzea eta moztea esaterako. Bertsio berriak mozteko erabilitako azken laukia gordetzen du, horrela, irudi anizkoitzak azkar moztu ditzakezu bata bestearen jarraian, neurri berdinarekin.

# Akats zuzenketak

## [Skanlite](https://kde.org/applications/eu/graphics/org.kde.skanlite) [2.2.0](http://download.kde.org/stable/skanlite/2.2/skanlite-2.2.0.tar.xz.mirrorlist)

* Gordetzea hari batera mugitu da, gordetzerakoan interfazea izoztu ez dadin

* D-Bus interfaze bat egin da laster-teklatarako eta eskaneatzea kontrolatzeko

## [Spectacle](https://kde.org/applications/eu/utilities/org.kde.spectacle)

* Spectacle-ren tenporizadore ezaugarria eta Maius + «Pantaila Inprimatu» (hartu pantaila osoko pantaila-argazkia) eta Meta + Maius + «Pantaila Inprimatu» (hartu eskualde laukizuzen bateko pantaila-argazkia) lasterbideak orain Wayland-en badabiltza.

* Spectacle-k aurrerantzean ez du saguaren kurtsorea era lehenetsian pantaila-argazkietan barneratzen.

## [Okteta](https://kde.org/applications/eu/utilities/org.kde.okteta) 0.26.4

* «struct2osd»ek orain «castxml» erabiltzen du («gccxml» gaitzetsi egin da).

* Qt kode zaharkitu gutxiago erabiltzen da, exekuzio garaian erregistratutako abisuak saihestuz.

* Itzulpenak hobetu dira.
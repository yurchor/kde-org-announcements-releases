---
draft: false
layout: page
publishDate: '2020-08-13T14:00:00+02:00'
summary: "Qu\xE8 ha passat a les aplicacions del KDE aquest mes"
title: "Actualitzaci\xF3 de les aplicacions del KDE d'agost de 2020"
type: announcement
---
El servei de llançaments de KDE ha efectuat una publicació nova amb dotzenes d'[aplicacions KDE](https://kde.org/applications/). Les funcionalitats noves, millores d'usabilitat, redissenys i correccions d'errors ajuden a augmentar la productivitat i fan que aquest lot nou d'aplicacions sigui més eficient i agradable d'usar.

Aquí hi ha el què podeu esperar:

## [Dolphin](https://kde.org/applications/ca/system/org.kde.dolphin)

{{< img class="text-center" src="dolphin_preview.webp" caption="El Dolphin mostra les vistes prèvies de molts tipus de fitxers." style="width: 800px" >}}

El Dolphin és l'explorador de fitxers del KDE que ajuda a cercar, copiar i obrir fitxers i carpetes. Com a tal, una de les seves funcions principals és donar una idea clara del què conté cada fitxer. Per això, és crucial la visualització de miniatures i el Dolphin ha estat capaç de mostrar vistes prèvies d'imatges, clips de vídeo, i inclús fitxers 3D del Blender des de fa temps.

En aquesta versió nova el Dolphin afegeix miniatures per a fitxers 3D Manufacturing Format (3MF) a la llista i també es poden mostrar vistes prèvies de fitxers i carpetes en sistemes de fitxers encriptats com les Caixes fortes del Plasma. Això es fa de forma segura emmagatzemant les miniatures a la memòria cau del sistema de fitxers en si, o recorrent a generar-les però sense emmagatzemar les versions a la memòria cau en qualsevol lloc, si és necessari. En qualsevol cas, encara hi ha un control absolut de com i quan el Dolphin mostra el contingut d'un fitxer, ja que es pot configurar independentment el llindar de la mida de fitxer per a mostrar vistes prèvies de fitxers locals i remots.

Una altra manera d'identificar fitxers és a través dels seus noms. En cas que el nom del fitxer sigui massa gran per a visualitzar, els desenvolupadors han refinat el comportament de l'escurçament del nom de fitxer. En lloc de retallar pel mig el nom llarg d'un fitxer i carpeta com feia el Dolphin a les versions anteriors, la versió nova retalla el final i sempre manté visible l'extensió del fitxer (si n'hi ha) després de l'el·lipsi. Això facilita molt la identificació de fitxers amb noms llargs.

El Dolphin ara recorda i restaura la ubicació que estàveu veient, així com les pestanyes obertes i les vistes dividides que estaven obertes en el darrer tancament. Aquesta funcionalitat està activada de manera predeterminada, però es pot desactivar a la pàgina «Inici» de la finestra «Configura» del Dolphin.

Parlant de les millores d'usabilitat, el Dolphin sempre ha permès muntar i explorar comparticions remotes, sigui via FTP, SSH, o altres protocols. Però ara el Dolphin mostra els muntatges remots i els FUSE amb noms de visualització fàcil d'emprar en lloc del camí complet. Encara millor, també es poden muntar imatges ISO usant un element nou de menú del menú contextual. Això vol dir que podeu baixar i explorar un sistema de fitxers que després gravareu en un disc o una memòria USB.

{{< video src="dolphin_ISO.mp4" caption="El Dolphin permet muntar imatges ISO." style="max-width: 900px" >}}

El Dolphin té una quantitat sorprenent de funcionalitats, però en podeu afegir encara més a través de connectors. Prèviament, si volíeu afegir connectors al menú «Servei» (veieu <I>Configuració</I> > <I>Configura el Dolphin</I> > <I>Serveis</I>) sovint caldrà executar un script o instal·lar un paquet de distribució. Ara es poden instal·lar directament des de la finestra «Obtén coses noves» sense passes manuals intermèdies. Altres canvis a la pàgina de configuració dels «Serveis» són que hi ha un camp nou de cerca a la part superior que ajuda a cercar ràpidament el que esteu cercant i la llista de serveis ara s'ordena alfabèticament.

El moviment d'elements és més fàcil amb el Dolphin nou, ja que ara hi ha accions noves per a moure ràpidament o copiar fitxers seleccionats en una subfinestra d'una vista dividida a l'altra subfinestra. Per a deixar clar que s'està fent, en arrossegar un fitxer ara el cursor canvia a una mà «agafada» de manera predeterminada, en lloc del cursor de còpia com es feia a les versions anteriors.

També es pot calcular i mostrar les mides de les carpetes al disc usant la vista «Detalls» i ara el plafó «Informació» del Dolphin mostra informació útil de la Paperera. També és possible navegar des del camp de cerca del Dolphin a la vista de resultats prement la tecla de fletxa avall.

Finalment, una funcionalitat nova útil és que el Dolphin 20.08 ve amb un element de menú nou «Copia la ubicació»...

## [Konsole](https://kde.org/applications/ca/system/org.kde.konsole)

{{< img class="text-center" src="konsolecolors.webp" caption="El Konsole té diverses funcionalitats noves excitants." style="width: 800px" >}}

... com a l'aplicació de terminal del KDE, el Konsole! Això vol dir que es pot fer un clic dret a un element que es mostri al Konsole, triar «Còpia la ubicació» des del menú emergent i després copiar i enganxar el camí complet del fitxer o directori a un document, un altre terminal, o una aplicació.

El Konsole també ve amb una funcionalitat nova que mostra un ressaltat subtil per a les línies noves que arriben a la vista quan la sortida del terminal es desplaça ràpidament i de manera predeterminada mostra una vista prèvia en miniatura dels fitxers d'imatge quan es passa per sobre amb el cursor.Encara més, quan es fa clic dret a un fitxer subratllat al Konsole, el menú contextual mostra un menú «Obre amb» de manera que es pot obrir el fitxer en qualsevol aplicació que es triï.

Des de fa temps hi havia la funcionalitat de dividir la vista del Konsole i afegir pestanyes per a fer diverses coses a la vegada, però ara es poden desactivar les capçaleres de la vista dividida, i es pot augmentar el gruix del separador. També es pot controlar una pestanya per al procés actiu a completar i assignar colors a les pestanyes del Konsole!

{{< img class="text-center" src="konsole-monitor.webp" caption="Un monitor nou de procés al Konsole" style="width: 500px" >}}

Parlant de pestanyes, s'ha eliminat la drecera predeterminada Ctrl+Majúscula+L del Konsole per a «Separa la pestanya actual». D'aquesta manera ja no és tan fàcil separar accidentalment la pestanya actual quan el que realment volíeu fer era netejar la pantalla usant Ctrl+Majúscula+K.

La millora d'usabilitat final en aquesta versió és que el cursor en forma d'I del Konsole ara s'adapta a la mida del tipus de lletra en comptes de romandre sempre en la mateixa mida.

## [Yakuake](https://kde.org/applications/ca/system/org.kde.yakuake)

{{< img class="text-center" src="yakuake.webp" caption="Icona nova del Yakuake" style="width: 200px" >}}

Parlant de terminals, el Yakuake és l'emulador de terminal desplegable del KDE. Es desplega des de la part superior de la pantalla cada vegada que es prem F12 i també té una pila millores per a aquest llançament. Per exemple, ara permet configurar totes les dreceres de teclat que venen del Konsole i hi ha un element nou de la safata del sistema que mostra quan el Yakuake s'està executant. Evidentment el podeu ocultar, però facilita veure si el Yakuake és actiu i si està preparat o no. També proporciona una manera gràfica d'invocar-lo.

També s'ha millorat el Yakuake al Wayland. La finestra principal apareixia sota els plafons de la part superior que es mostraven, però ara això s'ha corregit.

## [Digikam](https://kde.org/applications/ca/graphics/org.kde.digikam) 7.0.0

El DigiKam és l'aplicació professional de gestió fotogràfica del KDE i resulta que [l'equip acaba de publicar una versió major nova, la 7.0](https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4).

{{< img class="text-center" src="digikam-face.webp" caption="Reconeixement de les cares al Digikam" style="width: 800px" >}}

Un dels punts destacats principals del digiKam 7.0.0 és la millora del reconeixement facial, que ara usa algorismes d'intel·ligència artificial d'aprenentatge profund. Podeu veure la xerrada a l'Akademy de l'any passat per a veure com s'ha fet i hi haurà més feina relacionada en versions futures per al reconeixement i la detecció automàtica facial.

{{< video src="https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4" caption="Reconeixement de les cares al Digikam" style="max-width: 800px" controls="true" >}}

El [paquet Flatpak principal del digiKam està disponible a través de Flathub](https://flathub.org/apps/details/org.kde.digikam), així com una versió nocturna inestable per als provadors. El digiKam també està disponible per a Linux mitjançant les AppImage i els repositoris de les distribucions. També hi ha versions del digiKam per a Windows i Mac.

## [Kate](https://kde.org/applications/ca/utilities/org.kde.kate)

Hem fet un parell d'ajustos a la vista del nostre editor de text Kate. El menú «Obre recent», ara també mostra els documents oberts en el Kate des de la línia d'ordres i altres fonts, no només els que s'obren usant el diàleg de fitxer. Un altre canvi és que la barra de pestanyes del Kate ara és visualment coherent amb totes les altres barres de pestanyes de les altres aplicacions KDE i obre les pestanyes noves a la dreta, com ho fan la majoria de les altres barres de pestanyes.

## [Elisa](https://kde.org/applications/ca/multimedia/org.kde.elisa)

{{< img class="text-center" src="elisa.webp" caption="L'Elisa és un reproductor de música lleuger per a l'escriptori i el mòbil." style="width: 800px" >}}

L'Elisa és un reproductor de música lleuger que funciona a l'escriptori i al telèfon mòbil. L'Elisa ara permet visualitzar tots els gèneres, artistes, o àlbums a la barra lateral, sota altres elements. La llista de reproducció també mostra el progrés de la cançó que s'està reproduint en línia i la barra superior ara s'ajusta a la mida de la finestra i al factor de format del dispositiu. Això vol dir que té un bon aspecte amb una finestra/dispositiu amb orientació vertical (per exemple, al telèfon) i es pot reduir l'escala fins que sigui realment petita.

## [KStars](https://kde.org/applications/ca/education/org.kde.kstars) 3.4.3

{{< img class="text-center" src="kstars-main-screen.webp" caption="El KStars és l'aplicació KDE per als aficionats a l'astronomia." style="width: 800px">}}

Finalment, l'aplicació d'astronomia KStars ha guanyat més calibratge i enfocament a la [nova versió](http://knro.blogspot.com/2020/07/kstars-v343-is-released.html). [La màscara de Bahtinov és un dispositiu usat per a enfocar petits telescopis astronòmics amb precisió](https://en.wikipedia.org/wiki/Bahtinov_mask). És útil per a usuaris que no tenen un enfocador motoritzat i prefereixen enfocar manualment amb l'ajuda de la màscara. Després de capturar una imatge en el mòdul d'enfocament amb l'algoritme de la màscara de Bahtinov seleccionat, l'Ekos analitzarà les imatges i les estrelles dins seu. Si l'Ekos reconeix el patró d'estrella de Bahtinov, dibuixarà línies sobre el patró de l'estrella en cercles al centre i sobre un desplaçament per a indicar el focus.

{{< img class="text-center" src="kstars-focus.webp" caption="Un nou algorisme per a l'enfocament al KStars" style="width: 600px">}}

{{< img class="text-center" src="kstars-calibration.webp" caption="Calibratge al KStars" style="width: 600px" >}}

Hem afegit una subpestanya «Traçat del calibratge» a la dreta del «Traçat de la deriva». Mostra les posicions de muntura registrades durant el calibratge del guiador intern. Si les coses van bé, hauria de mostrar punts en dues línies que estiguin en angle recte entre si: una quan el calibratge empeny la muntura cap endavant i cap enrere al llarg de la direcció AR, i després quan farà el mateix per a la direcció Dec. No hi ha molta informació, però pot ser útil veure-la. Si les dues línies estan en un angle de 30 graus. Quelcom no va bé amb el calibratge! A dalt hi ha una fotografia en acció usant el simulador.

El [KStars està disponible per a baixada](https://edu.kde.org/kstars/) per a Android, des de la botiga Snapcraft, Windows, macOS i des dels repositoris de les distribucions de Linux.

## [KRDC](https://kde.org/applications/ca/internet/org.kde.krdc)

{{< video src="krdc.mp4" caption="El KRDC amb cursor remot" style="max-width: 900px" >}}

El KRDC permet veure o controlar la sessió d'escriptori d'una altra màquina. La versió publicada avui ara mostra els cursors adequats a la banda del servidor en el VNC en lloc d'un petit punt amb el cursor remot retardat al darrere seu.

## [Okular](https://kde.org/applications/ca/graphics/org.kde.okular)

L'Okular és el visor de documents del KDE. Permet llegir documents PDF, llibres ePUB i molts més tipus de fitxers basats en text. S'ha incorporat una correcció per a tornar a reunir «Impressió» i «Vista prèvia d'impressió» al menú «Fitxer».

## [Gwenview](https://kde.org/applications/ca/graphics/org.kde.gwenview)

El Gwenview és una aplicació de visualització d'imatges que té diverses funcionalitats bàsiques d'edició, com la redimensió i l'escapçat. La versió nova desa la mida de l'últim quadre usat d'escapçat. Això vol dir que es pot escapçar ràpidament múltiples imatges a la mateixa mida en una successió ràpida.

# Correcció d'errors

## [Skanlite](https://kde.org/applications/ca/graphics/org.kde.skanlite) [2.2.0](http://download.kde.org/stable/skanlite/2.2/skanlite-2.2.0.tar.xz.mirrorlist)

* El desament s'ha mogut a un fil per a no congelar la interfície mentre es desa

* S'ha implementat una interfície de D-Bus per a les dreceres de teclat i controlar de l'escaneig

## [Spectacle](https://kde.org/applications/ca/utilities/org.kde.spectacle)

* La característica de temporitzador de l'Spectacle i les dreceres Majúscula + ImprPantalla (pren una captura de pantalla completa) i Meta + Majúscula + ImprPantalla (pren una captura de pantalla d'una regió rectangular) ara funcionen al Wayland.

* L'Spectacle ja no inclou de manera predeterminada el cursor del ratolí a les captures de pantalla.

## [Okteta](https://kde.org/applications/ca/utilities/org.kde.okteta) 0.26.4

* «struct2osd» ara usa «castxml» («gccxml» ha quedat en obsoleta).

* Ús de codi Qt menys obsolet, evitant avisos registrats en temps d'execució.

* S'han millorat les traduccions.
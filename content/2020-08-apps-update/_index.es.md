---
draft: false
layout: page
publishDate: '2020-08-13T14:00:00+02:00'
summary: "\xBFQu\xE9 ha ocurrido este mes en las Aplicaciones de KDE?"
title: "Actualizaci\xF3n de las Aplicaciones de KDE de agosto de 2020"
type: announcement
---
El servicio de lanzamientos de KDE contiene docenas de nuevos lanzamientos de [aplicaciones de KDE](https://kde.org/applications/). Nuevas funcionalidades, mejoras de usabilidad, rediseños y corrección de errores contribuyen a aumentar la productividad y hacen que este nuevo lote de aplicaciones sea más eficiente y agradable de usar.

Esto es lo que puede esperar:

## [Dolphin](https://kde.org/applications/en/system/org.kde.dolphin)

{{< img class="text-center" src="dolphin_preview.webp" caption="Dolphin muestra vistas previas de numerosos tipos de archivos." style="width: 800px" >}}

Dolphin es el explorador de archivos de KDE que le ayuda a buscar, copiar y abrir archivos y carpetas. Como tal, una de sus funciones principales es proporcionarle una idea clara de lo que contiene cada archivo. Mostrar miniaturas es crucial para ello y Dolphin siempre ha mostrado vistas previas de imágenes, clips de vídeo e incluso archivos 3D de Blender.

En esta nueva versión, Dolphin añade a la lista miniaturas para archivos <i>3D Manufacturing Format</i> (3MF), además de vistas previas de archivos y carpetas en sistemas de archivos cifrados, como las <i>cajas fuertes</i> de Plasma. Para ello, las miniaturas en caché se guardan de forma segura en el propio sistema de archivos, o bien se generan sin guardar versiones en caché en ningún sitio como último recurso, si es necesario. De cualquier modo, existe un control absoluto sobre cuándo y cómo muestra Dolphin el contenido de los archivos, ya que puede configurar de forma independiente el límite de tamaño para que se muestren vistas previas de archivos locales y remotos.

Otro modo de identificar los archivos es a través de sus nombres. En caso de que el nombre del archivo a mostrar sea demasiado largo, los desarrolladores han refinado el comportamiento del acortamiento de su nombre. En lugar de acortar por la mitad los nombres largos de archivos y carpetas, como se hacía en versiones anteriores de Dolphin, ahora se acortan por el final, manteniendo siempre visible la extensión del archivo (si tiene alguna) tras los puntos suspensivos. Esto facilita la identificación de archivos con nombres largos.

Dolphin recuerda y restaura ahora la ubicación que estaba viendo la última vez que lo usó, así como las pestañas que tenía abiertas y las vistas divididas que estaba usando. Esta funcionalidad está activada por omisión, pero se puede desactivar en la página «Inicio» de la ventana de preferencias de Dolphin.

Hablando de mejoras de usabilidad, Dolphin siempre le había permitido montar y explorar recursos compartidos remotos, ya fuera usando FTP, SSH u otros protocolos. Ahora, Dolphin también muestra recursos remotos y FUSE montados con nombres amigables en lugar de con su ruta completa. E incluso mejor: también puede montar imágenes ISO usando una nueva opción en el menú de contexto. Esto significa que se puede descargar y explorar un sistema de archivos que más tarde se grabará en un disco o en una memoria USB.

{{< video src="dolphin_ISO.mp4" caption="Dolphin le permite montar imágenes ISO." style="max-width: 900px" >}}

Dolphin dispone de una sorprendente cantidad de funcionalidades, pero todavía se le pueden añadir más mediante complementos. Anteriormente, si se deseaba añadir complementos al menú «Servicio» (consulte <i>Preferencias</i> > <i>Configurar Dolphin</i> > <i>Servicios</i>), solía ser necesario ejecutar un script o instalar un paquete de la distribución. Ahora se pueden instalar directamente en la ventana de «Obtener novedades» sin pasos manuales intermedios. Otros cambios en la página de preferencias de «Servicios» son la existencia de un nuevo campo de búsqueda en la parte superior para ayudarle a encontrar rápidamente lo que esté buscando y que la lista de servicios se muestra ahora ordenada alfabéticamente.

El nuevo Dolphin permite mover elementos con más facilidad, ya que ahora dispone de nuevas acciones para mover o copiar rápidamente los archivos seleccionados en un panel de una vista dividida al otro panel. Para que esté claro lo que se está haciendo al arrastrar un archivo, el cursor cambia a una mano cerrada de forma predeterminada en lugar de al cursor de copia, como se hacía en versiones anteriores.

También se pueden calcular y mostrar los tamaños de las carpetas del disco usando la vista de «Detalles», y el panel de «Información» de Dolphin muestra ahora información útil para la papelera. Ahora también se puede ir desde el campo de búsqueda de Dolphin hasta la vista de resultados pulsando la tecla de «flecha abajo».

En último lugar, otra útil funcionalidad nueva es que Dolphin 20.08 contiene una nueva opción de menú «Copiar ubicación».

## [Konsole](https://kde.org/applications/en/system/org.kde.konsole)

{{< img class="text-center" src="konsolecolors.webp" caption="Konsole contiene varias nuevas e interesantes funcionalidades." style="width: 800px" >}}

... Como en la aplicación de terminal de KDE, ¡Konsole! Esto significa que puede hacer clic con el botón derecho del ratón sobre cualquier elemento que se muestre en Konsole y usar la opción «Copiar ubicación» del menú emergente para copiar y pegar la ruta completa del archivo o directorio en un documento, en otro terminal o en una aplicación.

Konsole también contiene una nueva funcionalidad que muestra un resaltado sutil para las nuevas líneas que sean visibles cuando la salida del terminal se desplaza rápidamente y muestra de forma predeterminada una vista previa en miniatura para los archivos de imagen sobre los que sitúe el cursor. Y todavía más, al pulsar con el botón derecho sobre un archivo subrayado en Konsole, el menú de contexto muestra el submenú «Abrir con» para que pueda abrir el archivo con cualquier aplicación de su elección.

Durante mucho tiempo ha existido la posibilidad de dividir la vista de Konsole y añadir pestañas para hacer varias cosas al mismo tiempo. Ahora se pueden desactivar las cabeceras de las vistas divididas y se puede aumentar el grosor del separador. También se puede monitorizar una pestaña para detectar la finalización del proceso activo y asignar colores a las pestañas de Konsole.

{{< img class="text-center" src="konsole-monitor.webp" caption="Nuevo monitor de procesos de Konsole" style="width: 500px" >}}

Hablando de pestañas, se ha eliminado el acceso rápido de teclado por omisión de Konsole para «Desprender la pestaña actual», Ctrl+Mayúsculas+L, debido a que resultaba demasiado fácil desprender la pestaña actual de forma accidental cuando lo que realmente quería hacer era borrar la pantalla usando Ctrl+Mayúsculas+K.

La última mejora de usabilidad de esta versión es que el cursor vertical de Konsole se adapta ahora al tamaño del tipo de letra en lugar de tener siempre el mismo tamaño.

## [Yakuake](https://kde.org/applications/en/system/org.kde.yakuake)

{{< img class="text-center" src="yakuake.webp" caption="Nuevo icono de Yakuake" style="width: 200px" >}}

Hablando de terminales, Yakuake es el emulador de terminal desplegable de KDE. Se despliega desde la parte superior de la pantalla cada vez que pulse F12 y también contiene un buen número de mejoras en este lanzamiento. Por ejemplo, ahora le permite configurar todos los accesos rápidos de teclado que provienen de Konsole y también existe un nuevo elemento en la bandeja del sistema que le muestra si Yakuake está en ejecución. Puede ocultarlo, por supuesto, pero resulta de utilidad para saber si Yakuake está activo y preparado o no. También proporciona un modo gráfico para invocarlo.

También se ha mejorado Yakuake en Wayland. La ventana principal solía aparecer debajo de los paneles superiores que estuviera mostrando, pero eso se ha corregido ahora.

## [Digikam](https://kde.org/applications/en/graphics/org.kde.digikam) 7.0.0

DigiKam es la aplicación de gestión de fotos profesional de KDE y resulta que [el equipo acaba de publicar una nueva versión mayor, la 7.0](https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4).

{{< img class="text-center" src="digikam-face.webp" caption="Reconocimiento de caras de Digikam" style="width: 800px" >}}

Uno de los puntos fuertes de digiKam 7.0.0 es la mejora del reconocimiento de caras, que ahora usa algoritmos de IA de aprendizaje profundo. Puede ver la charla de la Akademy del año pasado para saber cómo se lleva a cabo. También se realizará más trabajo relacionado con el reconocimiento de caras y su detección automática en futuras versiones.

{{< video src="https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4" caption="Reconocimiento de caras de Digikam" style="max-width: 800px" controls="true" >}}

El [paquete Flatpak principal de digiKam está disponible en Flathub](https://flathub.org/apps/details/org.kde.digikam), así como una compilación nocturna no estable para quien desee probarla. DigiKam también está disponible para Linux mediante AppImages y en los repositorios de las distribuciones. También existen versiones de digiKam para Windows y Mac.

## [Kate](https://kde.org/applications/en/utilities/org.kde.kate)

Hemos realizado varias modificaciones al aspecto de nuestro editor de texto Kate. El menú «Abrir reciente» también muestra ahora los documentos abiertos en Kate mediante la línea de órdenes y otros métodos, no solo los que se hayan abierto usando el diálogo de archivos. Otro cambio es que la barra de pestañas de Kate es ahora visualmente consistente con otras barras de pestañas de otras aplicaciones de KDE y abre las nuevas pestañas a la derecha, como hacen la mayoría de barras de pestañas.

## [Elisa](https://kde.org/applications/en/multimedia/org.kde.elisa)

{{< img class="text-center" src="elisa.webp" caption="Elisa es un reproductor de música ligero para el escritorio y para el móvil." style="width: 800px" >}}

Elisa es un reproductor de música ligero que funciona en el escritorio y en el teléfono móvil. Elisa le permite ahora mostrar todos los géneros, artistas y álbumes en la barra lateral, debajo de otros elementos. La lista de reproducción también muestra el avance de la canción que se está reproduciendo y la barra superior responde a los cambios del tamaño de la ventana y del factor de forma. Esto significa que se ve bien en una ventana o dispositivo en modo retrato (por ejemplo, en el teléfono móvil) y que se puede escalar hasta un tamaño realmente diminuto.

## [KStars](https://kde.org/applications/en/education/org.kde.kstars) 3.4.3

{{< img class="text-center" src="kstars-main-screen.webp" caption="KStars es la aplicación de KDE para los aficionados a la astronomía." style="width: 800px">}}

Finalmente, la aplicación de astronomía KStars ha añadido posibilidades de calibración y de enfoque en la [nueva versión](http://knro.blogspot.com/2020/07/kstars-v343-is-released.html). [La máscara de Bahtinov es un dispositivo usado para enfocar de forma precisa pequeños telescopios astronómicos](https://es.wikipedia.org/wiki/Máscara_de_Bahtinov). Resulta útil para usuarios que no disponen de un sistema de enfoque motorizado y prefieren enfocar manualmente con la ayuda de la máscara. Tras capturar una imagen en el módulo de enfoque con el algoritmo de máscara de Bahtinov seleccionado, Ekos analizará la imagen y las estrellas que contiene. Si Ekos reconoce el patrón de estrellas de Bahtinov, trazará líneas sobre el patrón de estrellas en círculos en el centro y en un desplazamiento para indicar el foco.

{{< img class="text-center" src="kstars-focus.webp" caption="Nuevo algoritmo de enfoque de KStars" style="width: 600px">}}

{{< img class="text-center" src="kstars-calibration.webp" caption="Calibración de KStars" style="width: 600px" >}}

Se ha añadido una pestaña «Gráfico de calibración» a la derecha de «Gráfico de arrastre». Muestra las posiciones de la montura grabadas durante la calibración del sistema de guiado interno. Si todo va bien, debería mostrar puntos en dos líneas que forman un ángulo recto entre sí: una cuando la calibración mueve la montura atrás y adelante en la dirección de AR, y luego cuando se hace lo mismo en la dirección de DEC. No es mucha información, pero verla puede resultar de ayuda. Si las dos líneas forman un ángulo de 30 grados, algo no está yendo bien durante la calibración. Arriba se muestra una imagen de este gráfico en acción usando el simulador.

[KStars está disponible para su descarga](https://edu.kde.org/kstars/) para Android, en la tienda Snapcraft, para Windows, macOS y, por supuesto, en los repositorios de las distribuciones de Linux.

## [KRDC](https://kde.org/applications/en/internet/org.kde.krdc)

{{< video src="krdc.mp4" caption="KRDC con cursor remoto" style="max-width: 900px" >}}

KRDC permite ver y controlar la sesión de escritorio de otra máquina. La versión que se publica hoy muestra cursores correctos del lado del servidor en VNC en lugar de un punto diminuto con el cursor remoto a la zaga.

## [Okular](https://kde.org/applications/en/graphics/org.kde.okular)

Okular es el visor de documentos de KDE. Le permite leer documentos PDF, libros ePUB y muchos más tipos de archivos basados en texto. Una corrección ha vuelto a poner «Imprimir» y «Vista previa de la impresión» una junto a la otra en el menú «Archivo».

## [Gwenview](https://kde.org/applications/en/graphics/org.kde.gwenview)

Gwenview es una aplicación para ver imágenes que contiene algunas funcionalidades básicas de edición, como cambiar el tamaño y recortar las imágenes. La nueva versión guarda el tamaño del último recuadro de recorte usado, lo que significa que puede recortar rápidamente múltiples imágenes al mismo tamaño en rápida sucesión.

# Corrección de errores

## [Skanlite](https://kde.org/applications/en/graphics/org.kde.skanlite) [2.2.0](http://download.kde.org/stable/skanlite/2.2/skanlite-2.2.0.tar.xz.mirrorlist)

* El proceso para guardar se ha movido a otro hilo para no congelar la interfaz del usuario al guardar.

* Se ha implementado una interfaz D-Bus para atajos de teclado y control del escaneo

## [Spectacle](https://kde.org/applications/en/utilities/org.kde.spectacle)

* La función del temporizador de Spectacle y los accesos rápidos de teclado Mayúsculas+ImprimirPantalla (captura de pantalla completa) y Meta+Mayúsculas+ImprimirPantalla (captura de área rectangular) ya funcionan en Wayland.

* Spectacle ya no incluye el cursor del ratón en las capturas de pantalla de forma predeterminada.

## [Okteta](https://kde.org/applications/en/utilities/org.kde.okteta) 0.26.4

* struct2osd usa ahora castxml (se ha discontinuado el uso de gccxml).

* Se usa menos código discontinuado de Qt, evitando el registro de advertencias en tiempo de ejecución.

* Se han mejorado las traducciones.
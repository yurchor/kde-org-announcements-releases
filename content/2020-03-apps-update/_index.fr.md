---
layout: page
publishDate: 2020-03-05 15:50:00
summary: Quoi de neuf ce mois-ci avec les applications de KDE
title: "Mise \xE0 jour des applications de KDE de Mars 2020"
type: announcement
---
## Nouvelles mises à jour

### Choqok 1.7

Février a démarré avec une mise à jour longtemps attendue pour notre application de microblogage [Choqok](https://choqok.kde.org/news/2020/02/04/choqok-1-7-released.html).

Les corrections importantes comprennent la limite à 280 caractères sur Twitter, permettant la désactivation des comptes et la prise en charge les tweets étendus.

{{< img class="text-center" src="quick-post.png" link="https://choqok.kde.org/" caption="Choqok" >}}

### KPMcore et KDE Partition Manager 4.1.0

Notre programme de formatage de disques, Partition Manager, a reçu une nouvelle mise à jour avec beaucoup de travaux réalisés sur la bibliothèque « KPMCore », utilisée par ailleurs par l'installateur de distributions « Calamares ». L'application elle-même prend en compte les systèmes de fichiers pour Minix.

{{< img class="text-center" src="kdepartitionmanager.png" link="https://kde.org/applications/system/org.kde.partitionmanager" caption="Partition Manager" >}}

### KPhotoAlbum 5.6

Si vous avez des centaines, voire des milliers de photos sur votre disque dur, il devient impossible de se souvenir de l'histoire derrière chaque image ou des noms de personnes sur la photo. KPhotoAlbum a été conçu pour vous aider à décrire vos photos et ensuite chercher dans l'immense tas de photos, rapidement et efficacement.

Cette mise à jour apporte tout particulièrement de très importantes améliorations de performance, lors de l'étiquetage d'un grand nombre d'images et aussi lors de l'affichage par vignettes. Félicitations à Robert Krawitz pour avoir plongé dans le code et trouvé des endroits pour des optimisations et la suppression de redondance !

De plus, cette version ajoute la prise en charge de l'environnement des modules externes pour les besoins de KDE.

[KPhotoAlbum](https://www.kphotoalbum.org/2020/02/08/0099/)

## Entrant

L'éditeur d'étiquettes « MP3 ID » Kid3 a été déplacé dans l'espace « kdereview », la première étape pour préparer de futures mises à jour. 

Et, l'application Rugola « pRocket.chat » est transférée dans l'espace « kdereview » et a été déplacée pour être prête pour une mise à jour. Cependant, le message du mainteneur Laurent Montel est qu'une ré-écriture avec un composant graphique « Qt Widgets » est prévue, ce qui représente un travail conséquent à réaliser. 

## Magasin d'applications

{{< img class="text-center" src="elisa.jpeg" link="https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab" caption="Elisa" >}}

Une nouvelle application arrive dans la boutique Microsoft Store pour Windows : [Elisa](https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab).

Un lecteur de musique moderne et élégant, réalisé avec amour par KDE. Naviguer dans votre musique et la jouer en toute facilité ! Elisa devrait pouvoir lire la plupart des fichiers de musique. Elisa est fait pour être accessible à tout le monde.

Pendant ce temps, Digikam est prêt pour être proposé sur la boutique Microsoft.

## Mises à jour des sites Internet

[KDE Connect](https://kdeconnect.kde.org/), un outil astucieux pour l'intégration de votre téléphone et votre bureau s'est doté d'un nouveau site Internet.

[Kid3](https://kid3.kde.org/) s'est doté d'un nouveau site Internet brillant avec des nouvelles et des liens de téléchargement pour toutes les boutiques pour lequel il est disponible.

## Mises à jour 19.12.3

Les mises à jour de certains projets se font selon leurs propres plannings et quelques uns sont mis à jour de façon synchronisée. Le groupe de projets pour la version -1912.3 a publié leurs mises à jour aujourd'hui. Celles-ci devraient être disponibles très bientôt dans les dépôts d'applications et dans les distributions. Veuillez consulter [la page de la mise à jour 19.12.3](https://www.kde.org/announcements/releases/19.12.3.php). Ce groupe était précédemment appelé les applications de KDE. Mais, ce terme a été abandonné pour devenir un service de mise à jour pour éviter la confusion avec les autres applications de KDE. La raison est qu'il y a des douzaines de logiciels différents, qui ne sont pas à considérer comme un ensemble unique.

Certaines de ces corrections incluses dans cette mise à jour sont :

* La prise en charge des attributs de fichiers pour les partages SAMBA a été améliorée par [de nombreuses contributions](https://cgit.kde.org/kio-extras.git/log/?h=release/19.12)

* K3b accepte maintenant des fichiers « WAV » comme fichiers audio [Bogue 399056](https://bugs.kde.org/show_bug.cgi?id=399056) [Contribution] (https://cgit.kde.org/k3b.git/commit/?h=release/19.12&id=2491cc70f9ffb129cf49633cbdc4f0d77789677d)

* Gwenview peut maintenant télécharger les images distantes via https [Contribution](https://cgit.kde.org/gwenview.git/commit/?h=release/19.12&id=9973a097d30a91dd50a4d23388b0b2b00eeeb626)

💻 [Notes pour la version
19.12](https://community.kde.org/Releases/19.12_Release_Notes) concernant les
paquets de source et les problèmes connus. 💻 [Page de tutoriel pour le
téléchargement de paquets]
(https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) 💻 [Page
d'informations sur les sources de la version
19.12.3](https://kde.org/info/releases-19.12.3) 💻 [Liste complète des
modifications pour la version 19.12] (https://kde.org/announcements/changelog-
releases.php?version=19.12.3) 💻
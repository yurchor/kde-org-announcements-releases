---
layout: page
publishDate: 2020-05-15 13:00:00
summary: Wat gebeurde er in de toepassingen van KDE deze maand
title: Update van toepassingen van KDE in mei 2020
type: announcement
---
# Nieuwe uitgaven

## Kid3

[Kid3](https://kid3.kde.org/) is een handig en krachtig programma voor tags aanbrengen die u de ID3-tags en soortgelijke formaten laat bewerken op MP3 en andere muziekbestanden.

Deze maand is het verplaatst om te gast te zijn bij KDE en heeft zijn eerste uitgave beleefd als een KDE toepassing. De uitgavenotitie zegt:

"Naast reparaties van bugs, biedt deze uitgave verbeteringen voor gebruik, extra sneltoetsen en scripts voor gebruikeracties. Speciale dank gaat naar verschillende mensen in KDE, die het gebruikersinterface en het handboek vertaald hebben naar nieuwe talen."

Kid3 is beschikbaar voor Linux, Windows, Mac en Android. U kunt het downloaden van de website of via uw distributie en de winkels Chocolatey, Homebrew en F-droid.

{{< img class="text-center" src="kid3-android.png" caption="Kid3 op Android" style="width: 351px">}}

{{< img class="text-center" src="kid3-linux.png" caption="Kid3 op Linux" style="width: 598px">}}

{{< img class="text-center" src="kid3-mac.png" caption="Kid3 op Mac"style="width: 586px" >}}

{{< img class="text-center" src="kid3-fdroid.png" caption="Kid3 is op FDroid" style="width: 319px">}}

{{< img class="text-center" src="kid3-homebrew.png" caption="Kid3 is op Homebrew" style="width: 660px">}}

{{< img class="text-center" src="kid3-chocolatey.png" caption="Kid3 is op Chocolatey" style="width: 175px">}}

## Calligra

[Calligra](https://calligra.org/news/calligra-3-2-0-released/index.html) is een kantoorsuite vol mogelijkheden en voldoet aan standaarden. Het heeft een aantal jaren geen uitgave gehad maar nu is het in stijl terug.

Versie 3.2 heeft Gemini, de kantoorsuite speciaal ontworpen voor 2-in-1 apparaten, dat wil zeggen, voor laptops met aanraakscherm die ook als tablets werken overgezet naar ons prachtige Kirigami framework. Het tekenprogramma Karbon ondersteunt nu documenten met meerdere pagina's. Stage, de Calligra bewerker van presentaties, ondersteunt nu automatische overgangen van dia's,

{{< img class="text-center" src="gemini-welcome.png" caption="Gemini" style="width: 400px">}}

{{< img class="text-center" src="karbon-complex.png" caption="Tekenen met Karbon" style="width: 400px">}}

{{< img class="text-center" src="stage.jpg" caption="Presentaties met Stage" style="width: 400px">}}

Calligra is beschikbaar via uw Linux distributie.

## Tellico 3.3 werkt gegevensbronnen bij

[Tellico](https://tellico-project.org/tellico-3-3-released/) is een beheerder van verzamelingen voor bijhouden van alles wat u verzamelt zoals boeken, bibliografieën, video's, muziek, videospellen, munten, postzegels, visitekaartjes, stripboeken en wijnen.

Om dit gemakkelijk te maken heeft het een lading gegevensengines om het gemakkelijk te maken te tonen wat u bezit. De nieuwe versie 3.3 van eerder deze maand voegt een gegevensbron toe voor [Colnect](https://colnect.com/) een samenvattende catalogus voor verzamelaars. Het werkt ook een aantal gegevensbronnen bij zoals Amazon en MobyGames.

{{< img class="text-center" src="colnect-search1.png" caption="Tellico met het Colnect gegevensmodel" >}}

# Inkomende projecten

Nieuwe projecten in KDE deze maand bevatten pvfviewer, een PC Stitch Pattern Viewer zodat u prachtige wandtapijten kunt maken. En Alligator, een RSS lezer die ons prachtige Kirigami framework gebruikt.

# KDE AppImage

Elke maand laten we graag een profiel zien van een van de nieuwe pakketcontainerformaten. AppImage is niet nieuw, het bestaat al meer dan tien jaar maar het is het waard ons er aan te herinneren dat een aantal KDE toepassingen het gebruiken.

## Wat is een AppImage?

AppImage is een pakketformaat die een manier voor bovenstroomse ontwikkelaars biedt om “inheemse” uitvoerbare programma's te leveren voor GNU/Linux gebruikers op dezelfde manier zoals ze kunnen doen voor andere besturingssystemen. Het biedt ingepakte toepassingen voor elk algemeen op Linux gebaseerd besturingssysteem, bijv., Ubuntu, Debian, openSUSE, RHEL, CentOS, Fedora etc. AppImages komen met alle afhankelijkheden waarvan niet aangenomen kan worden om deel te zijn van elk doelsysteem in een versie van recente datum en die zal draaien op de meeste Linux distributies zonder verdere wijzigingen.

## Waat u nodig hebt om te installeren om ze het beste te laten draaien?

Om een AppImage uit te voeren hebt u de Linux kernel (> 2.6) en `libfuse` nodig. Aan deze afhankelijkheid wordt voldaan in de meerderheid  van de GNU/Linux distributies zodat het niet nodig is dat u iets speciaals moet installeren.

Helaas is de ondersteuning van AppImage op de meeste bureaubladomgevingen (KDE en Gnome) is niet compleet zodat u een extra hulpmiddel nodig kan hebben om een menu-item van uw toepassing aan te maken. In zulke gevallen afhankelijk van de UX die uw voorkeur heeft kunt u kiezen tussen:

* [AppImageLauncher](https://www.appimagehub.com/p/1228228) voor integratie met een pop-up bij eerst-keer-uitvoeren of

* [appimaged](https://github.com/AppImage/appimaged) voor een automatische integratie van elke AppImage gebruikt in uw persoonlijke map.

Om uw AppImages bij te werken gebruikt u [AppImageUpdate](https://github.com/AppImage/AppImageUpdate). Deze is ingebed in AppImageLauncher dus als u het al hebt geïnstalleerd, is het niet nodig om iets extra's te installeren. Klik gewoon met rechts op het AppImage-bestand en kies bijwerken. Merk op dat niet alle pakketmakers de bijwerkinformatie in de uitvoerbare programma's inbedden zodat er gevallen kunnen zijn dat u handmatig de nieuwe versie moet downloaden.



## Welke kde toepassingen draaien ermee?

Er zijn verschillende KDE toepassingen die al beschikbaar zijn als AppImage, de meer relevante zijn:

* kdenlive

* krita

* kdevelop

## Wat is appimagehub?

De aanbevolen manier om AppImages te verkrijgen is van de originele auteurs van de toepassing, maar dit is niet erg praktisch als u nog steeds niet weet welke toepassing u nodig hebt. Dat is wanneer [AppImageHub](appimagehub.com) in zicht komt. Het is een softwarewinkel alleen toegewijd aan AppImages. U kunt daar een catalogus vinden met meer dan 600 toepassingen voor uw dagelijkse taken.

Deze website is onderdeel van het platform [OpenDesktop.org](opendesktop.org) die een compleet ecosysteem levert voor gebruikers en ontwikkelaars van vrije en open source toepassingen.

## Hoe maak je een AppImage?

Een AppImage maken is helemaal over bundelen van alle uw afhankelijkheden in een enkele map (AppDir). Daarna wordt het gebundeld in een sqaushfs-image en toegevoegd aan een 'runtime' die uitvoeren biedt.

Om deze taak uit te voeren kunt u de volgende hulpmiddelen gebruiken:

* https://github.com/linuxdeploy

* https://github.com/AppImageCrafters/appimage-builder

In de [AppImage Package Guide](https://docs.appimage.org/packaging-guide/index.html) kunt u een uitgebreide documentatie vinden over hoe zo'n hulpmiddel te gebruiken. U kunt ook deelnemen aan [AppImage IRC channel](irc://irc.freenode.net/#AppImage).



# 20.04.1 Uitgaven

Sommigen van onze projecten worden vrijgegeven in hun eigen tijdschaal en sommigen in massa vrijgegeven. De bundel 20.04.1 van projecten is vandaag vrijgegeven en zouden spoedig beschikbaar moeten zijn via opslagruimten voor toepassingen en distributies. Zie de [20.04.1 uitgavepagina](https://www.kde.org/info/releases-20.04.1.php) voor details.

Enige van de reparaties ingevoegd in deze uitgave zijn:

* kio-fish: wachtwoord alleen in KWallet opslaan als de gebruiker er om vraagt.

* De [Umbrello](https://umbrello.kde.org/) reparaties voor toevoegen van ondersteuning voor multiline c++.

* Het schuifgedrag in de documentviewer [Okular](https://kde.org/applications/en/graphics/org.kde.okular) is verbeterd en is bruikbaarder met vrij draaiend muiswiel

* Een regressie die soms de muziekspeler [JuK](https://kde.org/applications/en/multimedia/org.kde.juk) liet crachen bij opstarten is gerepareerd

* De [Kdenlive videobewerker](https://kde.org/applications/en/multimedia/org.kde.kdenlive) heeft veel updates gekregen voor stabiliteit, inclusief een reparatie aan het maken van een DVD chapter en een reparatie die het behandelen van tiijdcodes verbetert, verbeterde behandeling van ontbrekende clips, "photo" frame op afbeeldingsclips tekenen om te differentiëren van videoclips en vooruitblikken in de tijdlijn

* [KMail](https://kde.org/applications/en/office/org.kde.kmail2) behandelt nu op de juiste manier bestaande maildir-mappen bij toevoegen van een nieuw maildir-profiel en crasht niet langer bij toevoegen van teveel ontvangers

* Instellingen van [Kontact](https://kde.org/applications/en/office/org.kde.kontact) im- en exporteren is verbeterd door meer gegevens mee te nemen

[20.04 uitgavenotities](https://community.kde.org/Releases/20.04_Release_Notes)
&bull; [Wiki-pagina voor downloaden van
pakketten](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [20.04 informatiepagina van
broncode](https://kde.org/info/releases-20.04.0) &bull; [20.04 volledige log met
wijzigingen](https://kde.org/announcements/changelog-
releases.php?version=20.04.0)
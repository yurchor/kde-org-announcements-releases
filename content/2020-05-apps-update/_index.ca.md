---
layout: page
publishDate: 2020-05-15 13:00:00
summary: "Qu\xE8 ha passat a les aplicacions del KDE aquest mes"
title: "Actualitzaci\xF3 de les aplicacions del KDE de maig de 2020"
type: announcement
---
# Llançaments nous

## Kid3

El [Kid3](https://kid3.kde.org/) és un programa pràctic però potent d'etiquetatge de música que permet editar les etiquetes amb format ID3 i similars dels MP3 i altres fitxers de música.

Aquest mes s'ha mogut per a allotjar-se al KDE i ha fet el seu primer llançament com una aplicació KDE. La nota del llançament diu:

«A banda de la correcció d'errors, aquest llançament proporciona millores d'usabilitat, dreceres de teclat addicionals i scripts d'accions d'usuari. Agraïments especials a diversa gent de KDE, que han traduït la interfície d'usuari i el manual a idiomes nous."

El Kid3 està disponible per a Linux, Windows, Mac i Android. El podeu baixar des del lloc web o a través de la vostra distribució i de les botigues Chocolatey, Homebrew i F-droid.

{{< img class="text-center" src="kid3-android.png" caption="El Kid3 a l'Android" style="width: 351px">}}

{{< img class="text-center" src="kid3-linux.png" caption="El Kid3 a Linux" style="width: 598px">}}

{{< img class="text-center" src="kid3-mac.png" caption="El Kid3 a Mac"style="width: 586px" >}}

{{< img class="text-center" src="kid3-fdroid.png" caption="El Kid3 a FDroid" style="width: 319px">}}

{{< img class="text-center" src="kid3-homebrew.png" caption="El Kid3 és a Homebrew?" style="width: 660px">}}

{{< img class="text-center" src="kid3-chocolatey.png" caption="El Kid3 és a Chocolatey" style="width: 175px">}}

## Calligra

El [Calligra](https://calligra.org/news/calligra-3-2-0-released/index.html) és un paquet ofimàtic ple de funcionalitats i que compleix els estàndards. Ha estat sense publicar-se durant un parell d'anys però ara ha retornat amb estil.

La versió 3.2 té el Gemini, el paquet ofimàtic dissenyat especialment per als dispositius 2 en 1, és a dir, per a portàtils amb pantalles tàctils que poden funcionar com a tauletes, i que s'ha adaptat a l'enton de treball Kirigami. El programa de dibuix Karbon ara permet documents que continguin diverses pàgines. L'Stage, l'editor de presentacions del Calligra, ara permet transicions automàtiques de les dispositives.

{{< img class="text-center" src="gemini-welcome.png" caption="Gemini" style="width: 400px">}}

{{< img class="text-center" src="karbon-complex.png" caption="Dibuixant amb el Karbon" style="width: 400px">}}

{{< img class="text-center" src="stage.jpg" caption="Presentacions escèniques" style="width: 400px">}}

El Calligra està disponible a la vostra distribució Linux.

## El Tellico 3.3 actualitza les fonts de dades

El [Tellico](https://tellico-project.org/tellico-3-3-released/) és un gestor de col·leccions per a fer un seguiment de tot allò que col·leccioneu com llibres, bibliografies, vídeos, música, jocs de vídeo, monedes, segells, cromos, còmics i vins.

S'han carregat motors de dades per a fer més fàcil veure què teniu. La versió 3.3 nova d'aquest mes afegeix una font de dades per a [Colnect](https://colnect.com/), un catàleg ampli de col·leccionables. També actualitza una pila de fonts de dades com Amazon i MobyGames.

{{< img class="text-center" src="colnect-search1.png" caption="El Tellico amb el model de dades del Colnect" >}}

# Projectes entrants

Els projectes nous d'aquest mes al KDE inclouen el «pvfviewer», un visor de patrons de punt de creu per a PC, per a poder fer tapissos meravellosos. I l'Alligator, un lector RSS que usa l'entorn de treball Kirigami.

# AppImage del KDE

Cada mes ens agrada explicar un dels formats contenidors de paquets. L'AppImage no és nou, ja existeix des de fa un dècada, però val la pena recordar-lo ja que hi ha diverses aplicacions del KDE que l'usen.

## Què és una AppImage?

L'AppImage és un format d'empaquetament que proporciona una forma que els desenvolupadors de la línia principal subministrin binaris «natius» als usuaris de GNU/Linux de la mateixa manera que ho poden fer per a altres sistemes operatius. Permet empaquetar aplicacions per a qualsevol sistema operatiu basat en el Linux normal, p. ex. Ubuntu, Debian, openSUSE, RHEL, CentOS, Fedora, etc. Les AppImage vénen amb totes les dependències que no es poden assumir que formin part de cada un dels sistemes de destinació en una versió bastant recent i s'executaran a la majoria de les distribucions Linux sense més modificacions.

## Què cal instal·lar per a executar-les millor?

Per a executar una AppImage cal el nucli de Linux (> 2.6) i la `libfuse`. Les dependències estan presents a la majoria de les distribucions de GNU/Linux, i per tant no cal instal·lar res especial.

Lamentablement el funcionament de l'AppImage als entorns d'escriptori principals (KDE i Gnome) no és complet, i per tant, es requereix una eina addicional per a crear una entrada de menú per a l'aplicació. En aquests casos, en funció de l'UX preferida podreu triar entre:

* [AppImageLauncher](https://www.appimagehub.com/p/1228228) per a un menú emergent d'integració a la primera execució o

* [appimaged](https://github.com/AppImage/appimaged) per a una integració automatitzada de cada AppImage desplegada al directori personal.

Per a actualitzar les AppImage s'usa [AppImageUpdate](https://github.com/AppImage/AppImageUpdate). Està incrustat dins l'AppImageLauncher, de manera que si ja l'heu instal·lat, no cal instal·lar res més. Només cal fer un clic dret sobre el fitxer AppImage i triar actualització. Cal tenir present que no tots els empaquetadors incrusten la informació d'actualització dins els binaris, i per tant, poden haver-hi casos en que caldrà baixar manualment la versió nova.



## Quines aplicacions KDE s'hi executen?

Hi ha diverses aplicacions KDE que ja es distribueixen com a AppImage, i les més rellevants són:

* Kdenlive

* Krita

* KDevelop

## Què és Appimagehub?

La manera recomanada d'aconseguir les AppImage, és dels autors de l'aplicació original, però això no és gaire pràctic si encara no coneixeu quina aplicació necessiteu. Aquí és a on entra [AppImageHub](https://www.appimagehub.com). És una botiga de programari dedicada només a les AppImage. Allí hi trobareu un catàleg amb més de 600 aplicacions per a les tasques diàries.

Aquest lloc web forma part de la plataforma [OpenDesktop.org](https://www.opendesktop.org), la qual proporciona un ecosistema complet per a usuaris i desenvolupadors d'aplicacions de codi font lliure i obert.

## Com fer una AppImage?

Fer una AppImage consisteix en empaquetar totes les dependències de l'aplicació en un únic directori (AppDir). Després s'empaqueta en una imatge «squashfs» i s'afegeix a un «runtime» que permet la seva execució.

Per a portar a terme aquesta tasca es poden usar les eines següents:

* https://github.com/linuxdeploy

* https://github.com/AppImageCrafters/appimage-builder

A la [Guia de paquets AppImage](https://docs.appimage.org/packaging-guide/index.html) es pot trobar una documentació extensa de com usar aquestes eines. També podeu entrar al [canal IRC d'AppImage](irc://irc.freenode.net/#AppImage).



# Llançament 20.04.1

Diversos projectes publiquen en la seva pròpia escala de temps i altres es publiquen conjuntament. El paquet de projectes 20.04.1 s'ha publicat avui i aviat hauria de ser disponible a través de les botigues d'aplicacions i de les distribucions. Vegeu la [pàgina del llançament 20.04.1](https://www.kde.org/info/releases-20.04.1.php) per als detalls.

Algunes de les correccions incloses en aquest llançament són:

* kio-fish: Només emmagatzema la contrasenya al KWallet si l'usuari ho demana.

* L'[Umbrello](https://umbrello.kde.org/) corregeix el funcionament en afegir comentaris C++ multilínia.

* S'ha millorat el comportament del desplaçament al visor de documents [Okular](https://kde.org/applications/ca/graphics/org.kde.okular) i és més usable amb les rodes del ratolí de gir lliure

* S'ha corregit una regressió que a vegades provocava que el reproductor musical [JuK](https://kde.org/applications/ca/multimedia/org.kde.juk) fallés en iniciar-se

* L'[editor de vídeo Kdenlive](https://kde.org/applications/ca/multimedia/org.kde.kdenlive) ha rebut moltes actualitzacions d'estabilitat, incloent la correcció de la creació de capítols de DVD i la correcció que millora la gestió de codis de temps, millora la gestió de clips que manquen, el dibuixat del marc de «foto» als clips d'imatge per a diferenciar dels clips de vídeo, i les vistes prèvies a la línia de temps

* Ara el [KMail](https://kde.org/applications/ca/office/org.kde.kmail2) gestiona correctament les carpetes «maildir» existents en afegir un perfil «maildir» nou i ja no falla en afegir massa destinataris

* S'ha millorat la importació i l'exportació dels paràmetres del [Kontact](https://kde.org/applications/ca/office/org.kde.kontact) per a incloure més dades

[Notes de llançament de la versió
20.04](https://community.kde.org/Releases/20.04_Release_Notes) &bull; [Pàgina
wiki de baixada de
paquets](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;
[Pàgina d'informació del codi font de la versió
20.04.1](https://kde.org/info/releases-20.04.1) &bull; [Registre complet de
canvis de la versió 20.04](https://kde.org/announcements/changelog-
releases.php?version=20.04.1)
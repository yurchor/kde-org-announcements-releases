---
layout: page
publishDate: 2020-05-15 13:00:00
summary: Quoi de neuf ce mois-ci avec les applications de KDE
title: "Mise \xE0 jour des applications de KDE de Mai 2020 "
type: announcement
---
# Nouvelles mises à jour

## Kid3

[Kid3](https://kid3.kde.org/) est un élégant mais puissant programme de notation de musique, vous permettant de modifier les étiquettes « ID3 » et les formats similaires pour les fichiers de musique en « MP3 » ou autres.

Ce mois, il a été déplacé pour être hébergé par KDE et a publié sa première mise à jour comme application KDE. La note de mise à jour indique :

« En plus des corrections de bogues, cette mise à jour propose des améliorations fonctionnelles, des raccourcis additionnels de clavier et des scripts pour des actions de l'utilisateur. Un remerciement tout spécial à toutes les différentes personnes de KDE ayant traduit l'interface utilisateur et le manuel dans de nouvelles langues.  »

Kid3 est disponible pour Linux, Windows, Mac et Android. Vous pouvez le télécharger à partir du site Internet ou par votre distribution et les logithèques Chocolatey, Homebrew et F-droid.

{{< img class="text-center" src="kid3-android.png" caption="Kid3 sous Android" style="width: 351px">}}

{{< img class="text-center" src="kid3-linux.png" caption="Kid3 sous Linux" style="width: 598px">}}

{{< img class="text-center" src="kid3-mac.png" caption="Kid3 sur Mac"style="width: 586px" >}}

{{< img class="text-center" src="kid3-fdroid.png" caption="Kid3 est maintenant sur FDroid" style="width: 319px">}}

{{< img class="text-center" src="kid3-homebrew.png" caption="Kid3 est maintenant sur Homebrew" style="width: 660px">}}

{{< img class="text-center" src="kid3-chocolatey.png" caption="Kid3 est maintenant sur Chocolatey" style="width: 175px">}}

## Calligra

[Calligra](https://calligra.org/news/calligra-3-2-0-released/index.html) a acquis toutes ses fonctionnalités et respecte les standards des suites bureautiques. Il est resté sans mise à jour pendant quelques années mais il est de retour avec panache.

L'application Gemini, la suite bureautique spécifiquement conçue pour les périphériques 2 en 1 est passée en version 3.2. Elle est destinée aux portables avec écran tactile, pouvant se dédoubler en tablette, pris en charge par notre magnifique environnement de développement Kirigami. Le programme de dessin Karbon prend en charge maintenant les documents contenant de multiples pages. Concernant Stage, l'éditeur de présentation Calligra, prend en charge maintenant la transition automatique de diapositive.

{{< img class="text-center" src="gemini-welcome.png" caption="Gemini" style="width: 400px">}}

{{< img class="text-center" src="karbon-complex.png" caption="Dessin Karbon" style="width: 400px">}}

{{< img class="text-center" src="stage.jpg" caption="Présentations de Stage" style="width: 400px">}}

Calligra est disponible pour votre distribution Linux.

## Mises à jour des sources de données pour Tellico 3.3

[Tellico](https://tellico-project.org/tellico-3-3-released/) est un gestionnaire de collections pour conserver une trace de tout ce que vous collectionner comme les livres, les bibliographies, les videos, les musiques, les jeux vidéo, les pièces de monnaie, les timbres, les cartes de collection, les bandes dessinées et les vins.

Pour rendre ceci facile, le chargement de moteurs de données a été ajouté pour rendre facile l'affichage de ce que vous possédez. La nouvelle version 3.3 publiée plus tôt ce mois, ajoute une source de données pour [Colnect](https://colnect.com/), un catalogue exhaustif de ce qui est recherché par les collectionneurs. Il a mis aussi à jour un ensemble de source de données comme Amazon et MobyGames.

{{< img class="text-center" src="colnect-search1.png" caption="Tellico avec un modèle de données pour « Colnect »" >}}

# Nouveaux projets

Les nouveaux projets de KDE de ce mois accueille « pvfviewer », un afficheur de motifs de points. Ainsi, vous pouvez réaliser de splendides tapisseries, ainsi que Alligator, un lecteur de flux « RSS » utilisant notre magnifique environnement de développement Kirigami.

# AppImage de KDE

Chaque mois, KDE aime mettre en avant un des nouveaux formats de paquets. Le format « AppImage » n'est pas vraiment nouveau car il existe depuis plusieurs dizaines d'années. Mais, il vaut la peine de se rappeler à nous car un grand nombre d'applications de KDE l'utilise.

## Qu'est ce que AppImage ?

AppImage est un format de paquet fournissant un canal pour les développeurs en amont, pour livrer des exécutables « natifs » aux utilisateurs « GNU/Linux », de la même façon qu'ils le feraient pour d'autres systèmes d'exploitation. Il permet la mise en paquet d'applications pour tout système d'exploitation reposant sur un Linux standard, &pex; Ubuntu, Debian, openSUSE, RHEL, CentOS, Fedora etc. AppImage est livré avec toutes les dépendances, ne pouvant être garanties d'être intégrées dans le système cible dans une version assez récente. Il s'exécutera sur la plupart des distributions Linux, sans autre modifications.

## Qu'avez- vous besoin d'installer pour les faire fonctionner au mieux ?

Pour exécuter un paquet « AppImage », vous avez besoin d'un noyau Linux en version supérieure à 2.6 et de « libfuse ». Ces dépendances sont présentes dans le plupart des distributions GNU/Linux. Ainsi, vous n'avez besoin de n'installer rien de spécial.

Malheureusement, la prise en charge de AppImage sur les principaux environnements de bureau (KDE et Gnome) est partielle. Ainsi, vous pourriez avoir besoin d'outils supplémentaires pour créer une entrée de menu dans votre application. Dans un tel cas selon la version Linux que vous préférez, vous pouvez choisir entre :

* [AppImageLauncher](https://www.appimagehub.com/p/1228228) pour un menu contextuel d'intégration pour la première fois

* [appimaged](https://github.com/AppImage/appimaged) pour une intégration automatisée de chaque paquet « AppImage » installé dans votre dossier personnel.

Pour mettre à jour vos paquets « AppImage », vous pouvez utiliser [AppImageUpdate](https://github.com/AppImage/AppImageUpdate). Ceci est intégré dans le lanceur de « AppImage ». Ainsi, si vous l'avez déjà installé, vous n'avez besoin de ne rien installer d'autre. Simplement faites un clic droit sur le fichier « AppImage » et choisissez « Mise à jour ». Veuillez noter que tous les responsables de paquets n'intègrent pas les informations de mise à jour dans les exécutables. Ainsi, ils pourraient y avoir des cas dans lesquels vous aurez à charger manuellement la nouvelle version.



## Quelles sont les applications KDE qui fonctionnent avec eux ?

Il y a plusieurs applications qui ont déjà été distribuées comme AppImage. Les plus importantes sont :

* Kdenlive

* Krita

* KDevelop

## Qu'est-ce que appimagehub ?

La manière recommandée de trouver des paquets « AppImage » est d'avoir cette information par les développeurs des applications, mais cela est loin d'être pratique, surtout si vous ne savez pas quelle application vous avez besoin. Voilà la raison d'être de [AppImageHub](appimagehub.com), une boutique de logiciels réservée aux paquets « AppImage ». Avec elle, vous pouvez trouver un catalogue avec plus de 600 applications pour vos tâches quotidiennes.

Ce site Internet fait partie de la plate-forme [OpenDesktop.org](opendesktop.org), fournissant un écosystème complet pour les utilisateurs et les développeurs pour les applications libres et « Open Source ».

## Comment faire un AppImage ?

La réalisation d'un paquet « AppImage » se résume à rassembler toutes les dépendances de vos applications dans un unique dossier « AppDir ». Ensuite, il est rassemblée dans une image « squashfs » et complété par une bibliothèque d'exécution « runtime », permettant son exécution.

Pour réaliser cette tâche, vous pouvez utiliser les outils suivants : 

* https://github.com/linuxdeploy

* https://github.com/AppImageCrafters/appimage-builder

Dans le [Guide de paquet « AppImage »](https://docs.appimage.org/packaging-guide/index.html), vous pouvez trouver une documentation complète sur l'utilisation de tels outils. Ainsi, vous pouvez rejoindre le [Canal IRC « AppImage »](irc://irc.freenode.net/#AppImage).



# Mises à jour 20.04.1

Les mises à jour de certains projets se font selon leurs propres plannings et quelques uns sont mis à jour de façon synchronisée. Le groupe de projets pour la version 20.04.1 a publié leurs mises à jour aujourd'hui. Celles-ci devraient être disponibles très bientôt dans les dépôts d'applications et dans les distributions. Veuillez consulter [la page des mises à jour 20.04.1](https://www.kde.org/info/releases-20.04.1.php) pour plus de détails.

Certaines de ces corrections incluses dans cette mise à jour sont :

* kio-fish : ne stockez un mot de passe dans KWallet que si l'utilisateur le demande.

* Le modeleur UML, [Umbrello](https://umbrello.kde.org/) prend en charge maintenant les commentaires multi-lignes.

* Le comportement du défilement dans l'afficheur de document de [Okular](https://kde.org/applications/en/graphics/org.kde.okular) a été amélioré et est plus utilisable avec des molettes de souris à rotation libre.

* Une régression qui causait quelque fois un plantage du lecteur de musique [JuK](https://kde.org/applications/en/multimedia/org.kde.juk) au démarrage a été corrigé.

* Le [Éditeur de vidéo Kdenlive](https://kde.org/applications/en/multimedia/org.kde.kdenlive) a reçu de nombreuses mises à jour de stabilité, incluant une correction concernant la création de chapitre de DVD et une autre correction d'amélioration de la gestion des flux vidéos et des clips manquants, l'ajout d'un dessin de cadre de photos dans des clips vidéos pour les différencier des aperçus dans la piste vidéo.

* Maintenant, [KMail](https://kde.org/applications/en/office/org.kde.kmail2) gère correctement les dossiers existants de messagerie « maildir », lors de l'ajout d'un nouveau profil « maildir » et supprime un plantage lors de l'ajout d'un trop grand nombre de destinataires.

* Les paramètres d'importation et d'exportation de [Kontact](https://kde.org/applications/en/office/org.kde.kontact) ont été étendus pour prendre en compte plus de possibilités

[Notes de la version
20.04](https://community.kde.org/Releases/20.04_Release_Notes) • [Page de
tutoriel pour le téléchargement de
paquets](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) •
[Page d'informations sur les sources de la version
20.04.1](https://kde.org/info/releases-20.04.1) • [Liste complète des
modifications pour la version 20.04.1](https://kde.org/announcements/changelog-
releases.php?version=20.04.1)
---
layout: page
publishDate: 2019-12-12 13:01:00
summary: Quoi de neuf ce mois-ci avec les applications de KDE
title: "Mise \xE0 jour des applications de KDE de D\xE9cembre 2019 "
type: announcement
---
# Nouvelles versions des applications lancées en Décembre

La mise à jour de nouvelles versions pour les applications de KDE fait partie des efforts continue de KDE pour vous apporter un catalogue complet et à jour de programmes pleins de fonctionnalités, élégants et utiles à votre système.

Les nouvelles versions d'applications KDE sont maintenant disponibles concernant Dolphin, l'explorateur de fichiers, Kdenlive, l'un des éditeurs vidéo « Open source » le plus complet, Okular, l'afficheur de documents, Gwenview, l'afficheur d'images et tant d'autres applications et utilitaires favoris de KDE. Elles vous permettront d'être plus productif et créatif avec une utilisation facile et amusante des logiciels de KDE.

Nous espérons que vous apprécierez toutes les nouvelles fonctionnalités et améliorations apportées à toutes les applications de KDE !

## [Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan) re-travaillé

[Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan) a reçu une importante mise à jour en Décembre. Un énorme jalon a été atteint dans le planning du projet et dans les outils de gestion, puisque la version précédente a été diffusée il y presque deux ans.

{{< img class="text-center" src="kplan.png" link="https://dot.kde.org/sites/dot.kde.org/files/kplan.png" caption="Calligra Plan" >}}

Le logiciel Plan vous aider à gérer des projets petits et importants avec de nombreuses ressources. Pour que vous puissiez modéliser votre projet, il vous met à disposition différents types de dépendances de tâches et de contraintes de temps. Vous pouvez définir vos tâches, estimer les efforts nécessaires pour réaliser chacune, allouer des ressources et enfin planifier le projet selon vos besoins et les ressources disponibles.

L'une des forces de Plan réside dans son excellente prise en charge des diagrammes de Gantt. Ceux-ci sont des schémas constitués de barres horizontales, fournissant un affichage graphique du planning et d'aide à la planification, favorisant les coordinations et le suivi de tâches spécifiques dans un projet. L'utilisation de graphiques de Gantt vous aidera à une meilleure surveillance de votre flot d'informations du projet.

## Monter le volume sur [Kdenlive](https://kdenlive.org)

Les développeurs de [Kdenlive](https://kdenlive.org) ont ajoutés de nouvelles fonctionnalités et traqués les bogues à une vitesse vertigineuse. Cette version seule compte plus de 200 contributions logicielles (Commit).

Beaucoup de travail a été fait pour améliorer la prise en charge de l'audio. L'équipe de maintenance a résolu de nombreux problèmes comme une consommation très excessive de mémoire, le non-fonctionnement des vignettes audio ainsi qu'améliorer la vitesse de rangement.

Mais, encore plus incroyable, Kdenlive est maintenant livré avec un mixeur de sons tout à fait nouveau et spectaculaire (Voir l'image). Les développeurs ont aussi ajouté un nouvel affichage pour les clips audio dans le moniteur de clips et la corbeille du projet. Ainsi, vous pouvez mieux synchroniser vos films avec la bande son.

{{< img class="text-center" src="Kdenlive1912BETA.png" link="https://kdenlive.org/wp-content/uploads/2019/11/Kdenlive1912BETA.png" caption="Mixeur audio pour Kdenlive" >}}

L'équipe de résolution de problèmes de montage a amélioré à Kdenlive pour de meilleures performances et capacités d'utilisation. Les développeurs ont corrigé de nombreux bogues dans la version pour Windows de l'éditeur vidéo.

## [Dolphin](https://userbase.kde.org/Dolphin) : aperçus et navigation

Le puissant explorateur de fichiers de KDE, [Dolphin](https://userbase.kde.org/Dolphin), a reçu de nouvelles fonctionnalités pour vous aider à chercher et atteindre ce que vous voulez dans votre système de fichiers. Une de ces nouvelles fonctionnalités est la conception de nouvelles options avancées pour la recherche. Une autre est la possibilité qui vous est donnée de naviguer en avant et en arrière dans l'historique des emplacements que vous avez déjà visités plusieurs fois, activée par une appui long sur l'icône en forme de flèches dans la barre d'outils. La fonctionnalité qui vous permet d'atteindre rapidement les fichiers récemment enregistrés ou lus a été remaniée et ses défauts résolus.

L'une des préoccupations des développeurs de Dolphin est de permettre aux utilisateurs d'avoir un aperçu du contenu des fichiers avant de les ouvrir. Par exemple, dans la nouvelle version de Dolphin, vous pouvez avoir un aperçu des fichiers « gif » en les sélectionnant et ensuite en les survolant dans le panneau d'aperçu. Vous pouvez aussi cliquer et lancer des fichiers vidéos ou audio dans la panneau d'aperçu.

Si vous êtes un fan de BD, Dolphin peut maintenant afficher des vignettes pour les fichiers de Comic Books au format « .cb7 » (Vérifier la prise en charge de ce format aussi dans Okular -- Voir ci-dessous). Ensuite, concernant les vignettes, le zoom avant sur les vignettes a été un point ouvert pour Dolphin depuis longtemps. Maintenant, maintenez la touche <kbd>Ctrl</kbd> enfoncée, faites tourner la molette de la souris et la vignette va grossir ou se rétrécir. Une nouvelle fonctionnalité est que vous pouvez maintenant réinitialiser le zoom à son niveau par défaut en appuyant sur <kbd>Ctrl</kbd> + <kbd>0</kbd>.

Une autre fonctionnalité utile est l'affichage par Dolphin des programmes empêchant un périphérique d'être déconnecté.

{{<img src="dolphin.png" style="max-width: 500px" >}}

## [KDE Connect](https://community.kde.org/KDEConnect): donner le contrôle de votre bureau à votre téléphone

La dernière version de [KDE Connect](https://community.kde.org/KDEConnect) est fournie remplie de nouvelles fonctionnalités. L'une la plus remarquable est une nouvelle application de SMS vous permettant de lire et d'écrire vos SMS, tout en conservant un historique complet des conversations.

Une nouvelle interface utilisateur reposant sur [Kirigami](https://kde.org/products/kirigami/) est disponible vous permettant d'exécuter KDE Connect, non seulement sous Android mais aussi sur toutes les plate formes mobiles Linux qui arriveront avec des périphériques comme le PinePhone ou le Librem 5. L'interface de type Kirigami vous fournit aussi de nouvelles fonctionnalités pour les utilisateurs de bureau à bureau, comme le contrôle multimédia, la saisie à distance, la sonnerie du périphérique, le transfert de fichiers et l'exécution de commandes.

{{<img src="kdeconnect2.png" style="max-width: 600px" caption="La nouvelle interface pour le bureau" >}}

Vous pouvez déjà utilisé KDE Connect pour contrôler le volume du média en cours de lecture sur votre ordinateur. Mais, maintenant, vous pouvez aussi contrôler le volume global de votre système avec votre téléphone -- super-pratique lorsque que vous utilisez votre téléphone comme télé-commande. Lorsque vous êtes en discussion, vous pouvez aussi contrôler votre présentation en utilisant KDE Connect pour faire passer vos planches en avant ou en arrière.

{{<img src="kdeconnect1.png" style="max-width: 400px" >}}

Une autre fonctionnalité qui a été un point ouvert durant un moment est comment vous pouvez utiliser KDE Connect pour envoyer un fichier vers votre téléphone. Mais, ce qui est nouveau dans cette version est que votre téléphone va ouvrir immédiatement le fichier dès la fin de sa réception. KDE Itineray utilise ce mécanisme pour envoyer des informations de voyage vers votre téléphone via KMail.

Dans un note dédiée, maintenant, vous pouvez aussi envoyer des fichiers à partie de Thunar (Gestionnaire de fichiers de Xfce) et des applications Elementary comme Pantheon Files. L'affichage de l'avancement lors de la copie de plusieurs fichiers a été grandement amélioré et les fichiers reçus d'Android affichent maintenant l'heure correcte de modification.

Concernant les notifications, vous pouvez maintenant déclencher des actions à partir des notifications Android vers votre bureau. KDE Connect utiliser des fonctionnalités avancées du centre de notification de Plasma pour mettre à disposition de meilleures notifications. La notification de confirmation d'appairage n'est plus contrainte par une fin de temporisation, ainsi vous ne pourrez plus la manquer.

Enfin, si vous utilisez KDE Connect à partir de la ligne de commandes, les fonctionnalités de « kdeconnect-cli » a été maintenant améliorée pour l'auto-complètement « zsh ». 

## Images distantes [Gwenview](https://userbase.kde.org/Gwenview)

Le logiciel [Gwenview](https://userbase.kde.org/Gwenview) vous permet maintenant d'ajuster le niveau de compression « JPEG » lors de l'enregistrement des images modifiées. Les développeurs ont aussi améliorés les performances pour les images distantes. Gwenview est maintenant capable de traiter des photos vers ou à partir d'emplacements distants.

## [Okular](https://kde.org/applications/office/org.kde.okular) : prise en charge du format « .cb7 »

L'afficheur de documents [Okular](https://kde.org/applications/office/org.kde.okular) vous permet de lire, d'annoter ou de vérifier toute sorte de documents, incluant les formats « pdf », « epub », « odt » et « md » et prenant en charge aussi le format « cb7 » associé aux Comic Books. 

## [Elisa](https://kde.org/applications/multimedia/org.kde.elisa): lecteur de musique simple mais très complet

Le lecteur de musique [Elisa](https://kde.org/applications/multimedia/org.kde.elisa)combine la simplicité avec une interface moderne et élégante. Dans sa dernière version, Elisa a optimisé son apparence pour s'adapter beaucoup mieux aux écrans à haute résolution. Il s'intègre mieux avec les autres applications de KDE et fait l'objet d'une prise en charge par le système de menu global de KDE.

L'indexation des fichiers de musique a été aussi amélioré. Enfin, Elisa prend en charge aussi les radios Internet et fournit quelques exemples pour essayer.

{{< img src="elisa.png" caption="Interface mise à jour pour Elisa" >}}

## [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle) : prêt pour l'interface tactile

Le programme [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle) devient le programme par défaut pour la copie d'écran. Cette application de KDE a aussi évolué pour prendre en charge les interfaces tactiles. Son mode tactile avec le glissé rend plus facile la création d'un rectangle pour la capture des parties du bureau que vous avez choisies sur des périphériques tactiles. 

D'autres améliorations sont la nouvelle fonction d'enregistrement automatique, rendant agréable la prise rapide de multiples copies d'écran et les barres de progression animées, vous montrant clairement ce que Spectacle réalise.

{{<img style="max-width:500px" src="spectacle.png" caption="Nouveau gestionnaire pour le glisser sous Spectacle" >}}

## [Intégration du navigateur de Plasma](https://community.kde.org/Plasma/Browser_Integration) : améliorations

La nouvelle version pour [l'intégration du navigateur de Plasma](https://community.kde.org/Plasma/Browser_Integration) propose maintenant une fonction de liste noire pour la fonctionnalité de contrôle multimédia. Cela est utile quand vous visitez un site avec un grand nombre de média que vous pouvez lire et qu'il est difficile de choisir ce que vous voulez faire sur votre bureau. La liste noire vous permet d'exclure ces sites des contrôles de média.

La nouvelle version vous permet aussi de conserver l'URL d'origine dans les méta-données du fichier. Elle prend aussi en charge l'API « Web Share », permettant aux navigateurs de partager des liens, du texte et des fichiers avec d'autres applications, exactement comme le font les applications de KDE. Cela améliore l'intégration de toutes les applications de KDE avec les navigateurs non natifs que sont Firefox, Chrome / Chromium et Vivaldi.

{{<learn-more href="https://blog.broulik.de/2019/11/plasma-browser-integration-1-7/" >}}

## Boutique Microsoft

Quelques applications de KDE peuvent être téléchargées à partir de la boutique Microsoft. Les application sont présentes depuis quelques temps et sont rejointes maintenant par [Kile](https://kde.org/applications/office/org.kde.kile), un éditeur LaTeX convivial.

{{<microsoft-store href="https://www.microsoft.com/store/apps/9pmbng78pfk3" >}}

## Nouveau

Ce qui nous amène aux nouveaux arrivants : [SubtitleComposer](https://invent.kde.org/kde/subtitlecomposer), une application qui vous permet de créer facilement des sous-titres pour les vidéos. Elle fait partie de l'incubateur de KDE et est en route pour devenir un membre de plein droits de la famille des applications KDE. Bienvenue !

Pendant ce temps, une version minimale de Plasma, [plasma-nano](https://invent.kde.org/kde/plasma-nano), conçu pour les périphériques embarqués a été déplacé dans les dépôts de Plasma pour être livré avec la version 5.18.

## Mise à jour 19.12

Les mises à jour de certains projets se font selon leurs propres plannings et
quelques uns sont mis à jour de façon synchronisée. Le groupe de projets pour la
version 19.12 a publié leurs mises à jour aujourd'hui. Celles-ci devraient être
disponibles très bientôt dans les dépôts d'applications et dans les
distributions. Veuillez consulter [la page des mises à jour 19.12]
(https://www.kde.org/announcements/releases/19.12). Ce groupe était précédemment
appelé les applications de KDE. Mais, ce terme a été abandonné pour devenir un
service de mise à jour pour éviter la confusion avec les autres applications de
KDE. La raison est qu'il y a des douzaines de logiciels différents, qui ne sont
pas à considérer comme un ensemble unique.
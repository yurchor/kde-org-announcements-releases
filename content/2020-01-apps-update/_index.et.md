---
layout: page
publishDate: 2020-01-09 12:00:00
summary: What Happened in KDE's Applications This Month
title: KDE's January 2020 Apps Update
type: announcement
---
Aasta on nüüd 2020, me elame tulevikus, niisiis, vaatame, mida KDE rakendused meile sel kuul pakuvad!

## KTimeTracker porditi KDE Frameworks 5 peale

The long-awaited modernized version of KTimeTracker is finally released.The application is a personal time tracker for busy people which is nowavailable on Linux, FreeBSD and Windows. Over the course of 2019 it had beenported to Qt5 and KDE Frameworks after being unmaintained since around 2013.

{{< img class="text-center" src="ktimetracker.png" link="https://cdn.kde.org/screenshots/ktimetracker/ktimetracker.png" caption="KTimeTracker" >}}

The new version is also polished and slightly modernised with the mostnoticeable new features being the new Task Time Editing dialog andlive preview in the Export dialog as seen in the picture below.

{{< img class="text-center" src="ktimetracker-export.png" link="https://cgit.kde.org/ktimetracker.git/plain/doc/export-times-csv.png" caption="Export dialog in KTimeTracker" >}}

https://download.kde.org/stable/ktimetracker/5.0.1/src/It is available through your Linux distro or as a [Windows installer](https://download.kde.org/stable/ktimetracker/5.0.1/win64/) and there's even [untested MacOS builds](https://binary-factory.kde.org/job/KTimeTracker_Nightly_macos/) built nightly.

[Väljalaskest andis oma ajaveebis teada rakenduse hooldaja Alexander Potashev](https://aspotashev.blogspot.com/2019/12/ktimetracker-501-released.html)

## KStars 3.3.9

Astronoomiarakendus [KStars sai versioonis 3.3.9 uusi omadusi](http://knro.blogspot.com/2020/01/kstars-v339-is-released.html).

Piltidel saab muuta heledaid, tumedaid ja keskmisi toone, mis lubab näha ka kõige tuhmimaid tähti.

Lääne taevakultuuri vaimustavalt välja paistvad alternatiivsed tähtkujud ([loe lähemalt](https://sternenkarten.com/2019/12/27/kstars-clines/)).

{{< img src="Screenshot_20200102_162452.jpg" caption="Alternative constellations" alt="Western Sky Culture" >}}

[KStars on saadaval](https://edu.kde.org/kstars/) Androidile, Windowsile, MacOS-ile, Snapi hoidlas ja sinu Linuxi distributsiooonis.

## Frameworksi-ülene KNewStuff

Here on the Apps Update we focus on the apps rather than coding libraries. But new features in the common libraries will mean new features in all your apps :)

This month saw a redesigned UI for KNewStuff, the framework to download addons for your applications. The browse and download dialog was redesigned and the comments section can now be filtered. It'll be appearing in all your apps and settings modules shortly starting with the Global Theme module in System Settings.

{{< img src="knewstuff-comments.png" caption="Filters on Comments" alt="Filters on Comments" style="width: 800px">}}

{{< img src="knewstuff.png" caption="Redesigned Browse Dialog" alt="Redesigned Browse Dialog" style="width: 800px">}}

## Veaparandused

[KDevelopi igakuise veaparanduste uuenduse versiooniga 5.4.6](https://www.kdevelop.org/news/kdevelop-546-released) parandati ära pikemat aega muret valmistanud probleem, mis segas omavahel GPL ja LGPL päiseid. Paigalda see oma distributsioonis või Appimagest, et litsentsidega oleks jätkuvalt kõik korras.

[Latte Dock 0.9.7](https://psifidotos.blogspot.com/2019/12/latte-bug-fix-release-v097.html) parandas mõne omaduse probleemid Qt 5.14-ga ja kõrvaldas mõned krahhi põhjustavad asjad.

Dolphini pluginate väljalaskes 19.12.1 parandati ära SVN-i sissekandmise dialoog.

There was improved file indexing in Elisa. It also fixed some compilation issues on Android and without Baloo.

KPati uus väljalase on nüüd [OARS](https://hughsie.github.io/oars/index.html)-i vanusepiiranguteta.

Okularis on parandatud viga, mis tõi trükkimise eelvaatluse dialoogi sulgemisel kaasa krahhi.

Videoredaktori Kdenlive sellekuises väljalaskes on muljet avaldav hulk veaparandusi, millest parim on metainfos kasutatavate [ekraanipiltide](https://kde.org/applications/multimedia/org.kde.kdenlive) uuendamine. Samuti pakub see väljalase kümneid täiustusi ja parandusi ajateljel ja eelvaatluse käitlemisel.

Kate [LSP](https://langserver.org/) klient toetab nüüd JavaScripti.

## KDE viimaks Flathubi poes

KDE is embracing all the app stores. We can now deliver more and more of our programs directly to you the user. One of the leading app stores on Linux is [Flathub](https://flathub.org/home) which uses the FlatPak format.

You may well already have Flatpak and Flathub configured on your system and ready to use in Discover or other app installers. For example KDE neon has set it up by default on installs for over a year now. If not it's a quick [setup process](https://flatpak.org/setup/) for all the major distros.

Kui tunned huvi tehniliste üksikasjade vastu, võid sirvida [KDE Flatpaki paketihoidlat](https://phabricator.kde.org/source/flatpak-kde-applications/) ja lugeda [KDE Flatpaki juhiseid](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak).

Aga tõenäoliselt oled huvitatud lihtsalt rakendustest, millisel juhul vaata, mida pakub [Flathubi pood KDE puhul](https://flathub.org/apps/search/kde).

{{< img src="flathub.png" alt="KDE on Flathub" style="width: 800px" >}}

## LabPlot on nüüd Chocolateys

[Chocolatey](https://chocolatey.org/) is a package manager for Windows. If you want full control over what software is installed on your Windows machine or whole office of machines then Chocolatey gives you easy control over that just like you are used to on Linux.

[LabPlot](https://chocolatey.org/packages/labplot) is KDE's app for interactive graphing and analysis of scientific data and it is now available through Chocolatey. Give it a try!

[LabPloti ajaveeb](https://labplot.kde.org/2019/09/17/chocolatey-package-for-labplot-available/)

{{< img src="labplot.png" alt="LabPlot on Chocolatey" style="width: 800px" >}}

## Veebilehekülje uuendused

Hiljaaegu taassünni läbi elanud KDE veebimeeskond on asunud uuendama tervet hulka meie vanamoeliste teemadega lehekülgi. Üks armsamaid näiteid on vastselt uuenduskuuri läbi teinud fotode salvestamise ja otsimise rakenduse [KPhotoAlbum](https://www.kphotoalbum.org/)i veebilehekülg.

{{< img src="kphotoalbum.png" alt="New KPhotoAlbum website" >}}

Kui soovid aga näidata teistelegi hõlpsasti kasutatavat, aga siiski kõiki võimalusi pakkuvat muusikamängijat, ent oled seni häbenenud vanamoelise välimusega veebilehte, siis sai [JuK](https://juk.kde.org/) just äsja samuti tugevasti uuendatud veebilehe kujunduse.

## Väljalasked 19.12.1

Some of our projects release on their own timescale and some get released en-masse. The 19.12.1 bundle of projects was released today and should be available through app stores and distros soon. See the [19.12.1 releases page](https://www.kde.org/info/releases-19.12.1.php). This bundle was previously called KDE Applications but has been de-branded to become a release service to avoid confusion with all the other applications by KDE and because it is dozens of different products rather than a single whole.
---
layout: page
publishDate: 2020-01-09 12:00:00
summary: "O que aconteceu com o KDE Applications este m\xEAs"
title: "Atualiza\xE7\xE3o de janeiro de 2020 dos aplicativos da KDE"
type: announcement
---
Estamos em 2020 e vivendo o futuro, vamos ver o que os apps da KDE nos trouxeram neste último mês!

## KTimeTracker portado para o KDE Frameworks 5

A muito aguardada modernização do KTimeTracker foi enfim lançada. O aplicativo é um gerenciador de tempo pessoal para pessoas atarefadas que agora está disponível no Linux, FreeBSD e Windows. Ao longo de 2019, ele foi portado para o Qt5 e o KDE Frameworks após o período sem ser mantido desde 2013.

{{< img class="text-center" src="ktimetracker.png" link="https://cdn.kde.org/screenshots/ktimetracker/ktimetracker.png" caption="KTimeTracker" >}}

A nova versão também está polida e modernizada com as funcionalidades mais perceptíveis sendo a nova janela de Edição de tempo de tarefas e a pré-visualização na Janela de exportação como visto na figura abaixo.

{{< img class="text-center" src="ktimetracker-export.png" link="https://cgit.kde.org/ktimetracker.git/plain/doc/export-times-csv.png" caption="Janela de exportação no KTimeTracker" >}}

https://download.kde.org/stable/ktimetracker/5.0.1/src/ Disponível na sua distribuição Linux ou como [instalador do Windows](https://download.kde.org/stable/ktimetracker/5.0.1/win64/) e até mesmo como [uma build não testada para MacOS](https://binary-factory.kde.org/job/KTimeTracker_Nightly_macos/) feita diariamente.

[O lançamento foi anunciado no blog do mantenedor Alexander Potashev](https://aspotashev.blogspot.com/2019/12/ktimetracker-501-released.html)

## KStars 3.3.9

O programa de astronomia [KStars recebeu novas melhorias no 3.3.9](http://knro.blogspot.com/2020/01/kstars-v339-is-released.html).

Imagens agora possuem mudanças refinadas com Sombras, Tons médios e Realces, permitindo ver até a mais fraca das estrelas.

Constelações alternativas da Western Sky Cultura, algo fascinante [para se ler sobre](https://sternenkarten.com/2019/12/27/kstars-clines/).

{{< img src="Screenshot_20200102_162452.jpg" caption="Constelações alternativas" alt="Western Sky Culture" >}}

[O KStars está disponível para](https://edu.kde.org/kstars/) Android, Windows, MacOS, Snap Store e em sua distribuição Linux.

## Common Frameworks - KNewStuff

Aqui nas Atualizações de Apps nós focamos nos apps ao invés de bibliotecas para programação. Mas novas funcionalidades nas bibliotecas comuns também significam novas funcionalidades nos seus aplicativos. :)

Neste mês, foi reformulada a interface de usuário do KNewStuff, a infraestrutura para baixar extensões para seus aplicativos. A janela de navegação e download foi refeita e a seção de comentários agora pode ser filtrada. A interface aparece em todos os seus módulos de aplicativos e configurações logo após inicializar o módulo de Tema global nas Configurações do Sistema.

{{< img src="knewstuff-comments.png" caption="Filtros nos comentários" alt="Filters on Comments" style="width: 800px">}}

{{< img src="knewstuff.png" caption="Janela de navegação reformulada" alt="Redesigned Browse Dialog" style="width: 800px">}}

## Correções de erros

[A atualização mensal de correção de erros do KDevelop 5.4.6](https://www.kdevelop.org/news/kdevelop-546-released) arrumou um problema antigo onde os cabeçalhos GPL and LGPL se misturavam, atualize com sua distribuição ou com o AppImage para garantir que suas licenças estejam corretas.

[O Latte Dock 0.9.7](https://psifidotos.blogspot.com/2019/12/latte-bug-fix-release-v097.html) consertou certas funcionalidades do Qt 5.14 e removeu alguns crashes.

O Dolphin Plugins 19.12.1 teve uma janela de commit do SVN arrumada.

Houveram melhorias na indexação de arquivos do Elisa. Elas também repararam problemas de compilação no Android e sem o Baloo.

No novo lançamento do KPat, ele foi declarado como não tendo nenhuma restrição de idade relevante [OARS](https://hughsie.github.io/oars/index.html).

O Okular recebeu a correção de um crash que ocorria na janela de pré-visualização de impressão.

No lançamento do editor de vídeo Kdenlive deste mês teve um número impressionante de correções, uma das quais foi a atualização das [capturas de tela](https://kde.org/applications/multimedia/org.kde.kdenlive) usadas nas informações meta. Também há dezenas de melhorias e correções na linha do tempo e na manipulação da pré-visualização.

Novo suporte ao JavaScript agora está presente no cliente de [LSP](https://langserver.org/) do Kate.

## Aproveite programas da KDE na Flathub Store

A comunidade KDE está aceitando todas as lojas de aplicativos. Nós agora podemos fornecer mais e mais dos nossos programas diretamente para você. Uma das principais lojas de aplicativos no Linux é a [Flathub](https://flathub.org/home) que usa o formato FlatPak.

Você talvez já tenha o Flatpak e o Flathub configurados no seu sistema prontos para ser usados no Discover e outros instaladores de aplicativos. Por exemplo, o KDE neon vem com eles ativos por padrão em novas instalações faz mais de um ano. Caso não esteja, o [processo de instalação](https://flatpak.org/setup/) é rápido e simples em qualquer distribuição.

Se você estiver interessado nos detalhes mais técnicos você pode navegar no [repositório de pacotes Flatpak da KDE](https://phabricator.kde.org/source/flatpak-kde-applications/) e ler o [guia de Flatpak da KDE](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak).

Mas provavelmente você está mesmo interessado é nos aplicativos, então dê só uma olhada na [seção da KDE na loja Flathub](https://flathub.org/apps/search/kde).

{{< img src="flathub.png" alt="KDE on Flathub" style="width: 800px" >}}

## LabPlot agora no Chocolatey

O [Chocolatey](https://chocolatey.org/) é um gerenciador de pacotes para o Windows. Se você quiser controle total sobre que programas são instalados na sua máquina com Windows ou em escritórios inteiros, então o Chocolatey te dará o controle fácil de que você precisa do mesmo modo que você deve estar acostumado no Linux.

O [LabPlot](https://chocolatey.org/packages/labplot) é o aplicativo da KDE para plotagem interativa e análise de dados científicos e agora está disponível no Chocolatey. Aproveite para testar!

[Blog do LabPlot](https://labplot.kde.org/2019/09/17/chocolatey-package-for-labplot-available/)

{{< img src="labplot.png" alt="LabPlot on Chocolatey" style="width: 800px" >}}

## Atualizações dos sites

A recém-revivida equipe de Web da comunidade KDE tem atualizado vários dos nossos sites com temas antiquados. A nova página do [KPhotoAlbum](https://www.kphotoalbum.org/) é um ótimo exemplo disso agora que está atualizada e condizente com o aplicativo de armazenamento e pesquisa de fotos.

{{< img src="kphotoalbum.png" alt="New KPhotoAlbum website" >}}

Se você queria um reprodutor de música local, simples e completo mas ficou incomodado com a página antiga, o [JuK](https://juk.kde.org/) acabou de ser atualizado também.

## Lançamentos 19.12.1

Alguns dos nossos projetos lançam a seu próprio tempo e alguns são lançados em
lotes. O conjunto de projetos do 19.12.1 foi lançado hoje e deve ficar
disponível nas lojas de aplicativos e distribuições em breve. Dê uma olhada na
[página de lançamentos do 19.12.1]
(https://www.kde.org/info/releases/19.12.1.php). Este conjunto era chamado
anteriormente de KDE Applications mas teve sua nomeação alterada para se tornar
um serviço de lançamentos, deste modo evitando confusão com todos os outros
aplicativos da KDE e também por ser dezenas de produtos diferentes ao invés de
um conjunto só.